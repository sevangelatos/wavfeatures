#ifndef PLAIN_IMAGE_H
#define PLAIN_IMAGE_H

#define USE_OPENCV_CPP

#include <stdint.h>
#include <stdbool.h>

#ifdef USE_OPENCV_CPP
#include "opencv2/opencv.hpp"
#endif

#ifdef __cplusplus
 extern "C" {
#endif

typedef struct PlainImage_s *PlainImage;

struct PlainImage_s
{
    int width;                // Image width
    int height;               // Image height
    int bytes_per_sample;     // How many bytes do we accupy for each pixel? (=3 if RGB_bit8)
    int channels;             // Number of image channels
    uint8_t **pixels;         // The actual image data (actual sample data type might not be uchars)
    void *buffer;             // The original pointer that we should free.
    bool continuous;          // Are the image data allocated in a continuous buffer?
    void *_align[];           // Just to ease alignment of image data after the main struct
};

bool PlainImage_RunTests();

PlainImage PlainImage_Create(int width, int height, int bytes_per_sample, int channels);

#ifdef USE_OPENCV_CPP

bool PlainImage_ShallowCopyMat(PlainImage *pdst, cv::Mat &src);
bool PlainImage_DeepCopyMat(PlainImage *pdst, cv::Mat &src);

#endif

void PlainImage_Integral(PlainImage integral, PlainImage input);

void PlainImage_Min(PlainImage dest, PlainImage src1, PlainImage src2);

void PlainImage_Set(PlainImage dst, uint8_t val);

void PlainImage_AbsGrayDiff(PlainImage dst, PlainImage a, PlainImage b);

void PlainImage_Copy(PlainImage dst, PlainImage src);

void PlainImage_Destroy(PlainImage img);


#ifdef __cplusplus
 }
#endif

#endif // PLAIN_IMAGE_H
