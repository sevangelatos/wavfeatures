#include "opencv2/opencv.hpp"
#include <cstdio>
#include <iostream>
#include <stdint.h>
#include <algorithm>
#include <libgen.h>
#include "plain_image.h"
#include "videofeatureextractor.h"

#define CROSS_LENGTH   10
#define CIRCLE_RADIUS   3

using namespace cv;
using namespace std;
using namespace Haar;

void draw_cross(Mat *frame, CvPoint center);
static VideoCapture* OpenCaptureDev(char *filename);

void store_image(Mat &img)
{
    char filename[255];
    static int i = 0;
    sprintf(filename, "/tmp/img_%03d.png", ++i);
    imwrite(filename, img);
}

#if 0
static void scan_image(PlainImage integral, VideoFeatureExtractor &fe)
{
    int r, c;
    const int border = 16;
    const int step = 2;
        
    for (r = border;  r < integral->height - border; r += step) {
        for (c = border;  c < integral->height - border; c += step) {
            printf("%d, %d -> %d\n", r, c, fe.haar1(0, r, c));
        }
    }
    
}
#endif

static void print_feature_vector(char * filename, VideoFeatureExtractor &fe)
{
    int center_col = fe.width() / 2;
    int center_row = fe.height() / 2;

    int grad_h = fe.getMaxGradientH(10);
    int grad_v = fe.getMaxGradientV(10);

    int haar_2_a = fe.haar1(0, center_row, center_col);
    int haar_2_b = fe.haar1(1, center_row, center_col);

    int haar_4_a = fe.haar1(2, center_row, center_col);
    int haar_4_b = fe.haar1(3, center_row, center_col);

    int haar_2 = MIN(haar_2_a, haar_2_b);
    int haar_4 = MIN(haar_4_a, haar_4_b);

    printf("%s %llu %d %d %d %d\n", filename, fe.timestamp(), grad_h, grad_v, haar_2, haar_4);
}

bool haar_tests()
{
    bool result = true;
    int dim = 2;
    HaarLike1 haar1x1(1, 1, 1, 1, 0, 0);

    PlainImage gray4x4 = PlainImage_Create(dim, dim, 1, 1);
    PlainImage_Set(gray4x4, 0);
        
    gray4x4->pixels[0][0] = 11;
    gray4x4->pixels[0][1] = 12;
    gray4x4->pixels[1][0] = 21;
    gray4x4->pixels[1][0] = 22;
    
    // Create an integral image...
    PlainImage integral = PlainImage_Create(gray4x4->width + 1, gray4x4->height + 1, sizeof(int32_t), 1);
    PlainImage_Integral(integral, gray4x4);
    
    if (haar1x1.evaluate(integral, 0, 0) != -gray4x4->pixels[0][0]) {
        printf("%s:%d: test failed!\n", __FUNCTION__, __LINE__);
        result = false;
    }
    
    return result;
}

int main(int , char* argv[])
{
    char *filename = argv[1];
    // Open capture device
    Ptr<VideoCapture> capture = OpenCaptureDev(filename);

    PlainImage_RunTests();
    haar_tests();

    if(!capture || !capture->isOpened())  // check if we succeeded
        return -1;

    int width = capture->get(CV_CAP_PROP_FRAME_WIDTH);
    int height = capture->get(CV_CAP_PROP_FRAME_HEIGHT);
    
    VideoFeatureExtractor feature_extractor(width, height);
    Mat frame;
    Mat delta(height, width, CV_8UC1);
    Mat integral(height + 1, width + 1, CV_32SC1);

    // Init as black images
    delta = 0.0;
    integral = 0.0;

    if (filename) {
        filename = basename(filename);
    }
    else {
        filename = (char *) "-";
    }


    // Arrage to keep the actual data of delta and integral
    // in opencv matrices.
    PlainImage_ShallowCopyMat(&feature_extractor.delta(), delta);
    PlainImage_ShallowCopyMat(&feature_extractor.integral(), integral);

    // Create a window in which the captured images will be presented
    namedWindow("Camera");
    namedWindow("GrayDiff");
    namedWindow("Integral");
    
    feature_extractor.addFeature(HaarLike1(width, height, width/2, height, 0, 0));
    feature_extractor.addFeature(HaarLike1(width, height, width/2, height, width/2, 0));
    feature_extractor.addFeature(HaarLike1(width, height, width/2, height/2, 0, height/4));
    feature_extractor.addFeature(HaarLike1(width, height, width/2, height/2, width/2, height/4));

    // Get one frame at a time
    while(capture->read(frame)) {

        flip(frame, frame, 0);

        int timestamp = capture->get(CV_CAP_PROP_POS_MSEC);
        PlainImage_DeepCopyMat(&feature_extractor.expired(), frame);

        PlainImage upcoming_frame = feature_extractor.expired();

        feature_extractor.update(upcoming_frame, timestamp);
        
        print_feature_vector(filename, feature_extractor);

        //store_image(frame);
        //store_image(delta);
/*
        imshow("Camera", frame);
        imshow("Integral", integral);
        imshow("GrayDiff", delta);

        //If ESC key pressed exit
        if (27 == cv::waitKey(30)) {
            cerr << "Exit....\n";
            break;
        }
        */
    }

    destroyWindow("Camera");
    destroyWindow("GrayDiff");
    destroyWindow("Integral");

    return 0;
}

static VideoCapture* OpenCaptureDev(char *filename)
{
    if (filename == NULL) {
        return new VideoCapture(0);
    }
    else {
        cerr << "Using file: " << filename << endl;
        return new VideoCapture(filename);
    }
}

void draw_cross(Mat *frame, CvPoint center)
{
    // Draw horizontal line
    CvPoint pt1;
    CvPoint pt2;

    pt1.x = center.x - CROSS_LENGTH;
    pt1.y = center.y;

    pt2.x = center.x + CROSS_LENGTH;
    pt2.y = center.y;

    // Draw vertical line
    CvPoint pt3;
    CvPoint pt4;

    pt3.x = center.x;
    pt3.y = center.y - CROSS_LENGTH;

    pt4.x = center.x;
    pt4.y = center.y + CROSS_LENGTH;

    cvLine(frame, pt1, pt2, CV_RGB(0,255,0), 1, 8, 0);
    cvLine(frame, pt3, pt4, CV_RGB(0,255,0), 1, 8, 0);
    cvCircle(frame, center, CIRCLE_RADIUS, CV_RGB(255,0,0), -1, 8, 0 );

    return;
}
