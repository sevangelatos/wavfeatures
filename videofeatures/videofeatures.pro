TEMPLATE = app
CONFIG += console
CONFIG -= qt

INCLUDEPATH += "/opt/local/include"
LIBS += -L/opt/local/lib/ -lopencv_core -lopencv_highgui -lopencv_imgproc

SOURCES += main.cpp \
    plain_image.cpp \
    videofeatureextractor.cpp

HEADERS += \
    haar.h \
    plain_image.h \
    videofeatureextractor.h

