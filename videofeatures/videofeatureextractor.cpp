#include "videofeatureextractor.h"

VideoFeatureExtractor::VideoFeatureExtractor(int width, int height)
{
    m_rgb = PlainImage_Create(width, height, 3, 3);
    m_rgb_upcoming = PlainImage_Create(width, height, 3, 3);
    m_expired = PlainImage_Create(width, height, 3, 3);
    m_delta1 = PlainImage_Create(width, height, 1, 1);
    m_delta2 = PlainImage_Create(width, height, 1, 1);
    m_delta = PlainImage_Create(width, height, 1, 1);
    m_integral = PlainImage_Create(width + 1, height + 1, sizeof(int32_t), 1);
    m_col_min = 0;
    m_row_min = 0;
    m_col_max = width;
    m_row_max = height;
    
    reset();
}

VideoFeatureExtractor::~VideoFeatureExtractor()
{
    PlainImage_Destroy(m_rgb);
    PlainImage_Destroy(m_rgb_upcoming);
    PlainImage_Destroy(m_expired);
    PlainImage_Destroy(m_delta1);
    PlainImage_Destroy(m_delta2);
    PlainImage_Destroy(m_delta);
    PlainImage_Destroy(m_integral);
}

void VideoFeatureExtractor::update(PlainImage rgb_new, int64_t timestamp_new)
{
    PlainImage tmp = m_expired;

    // Update the timestamps
    m_timestamp = m_timestamp_upcoming;
    m_timestamp_upcoming = timestamp_new;

    // turn the images
    m_expired = m_rgb;
    m_rgb = m_rgb_upcoming;

    // Recycle the pointers if possible to avoid making deep copies.
    if (tmp == rgb_new) {
        m_rgb_upcoming = rgb_new;
    }
    else {
        std::printf("Warning: inefficient copying..\n");
        PlainImage_Copy(tmp, rgb_new);
        m_rgb_upcoming = tmp;
    }

    if (m_counter < 3) { m_counter += 1; }

    if (m_counter <= 1) {
        // We still have only one image
        return;
    }

    // Update deltas
    std::swap(m_delta1, m_delta2);
    PlainImage_AbsGrayDiff(m_delta1, m_rgb, m_rgb_upcoming);

    if (m_counter >= 3) {
        PlainImage_Min(m_delta, m_delta1, m_delta2);

        // Build the integral image
        PlainImage_Integral(m_integral, m_delta);
    }

}

void VideoFeatureExtractor::reset()
{
    PlainImage_Set(m_delta2, 255);
    PlainImage_Set(m_delta1, 255);
    PlainImage_Set(m_rgb_upcoming, 0);
    PlainImage_Set(m_rgb, 0);
    PlainImage_Set(m_integral, 0);
    m_timestamp = 0;
    m_timestamp_upcoming = 0;
    m_counter = 0;
}

int VideoFeatureExtractor::getMaxGradientH(int radious)
{
    int c, c2, grad;
    int r = m_integral->height - 1;
    int32_t max = -(1 << 30);
    
    int32_t *row = (int32_t *) m_integral->pixels[r];
    
    for (c = 0; (c2 = c + radious) < m_integral->width; c++) {
        
        if ((grad = row[c2] - row[c]) >= max) {
            max = grad;
        }
    }
    
    return max;
    
}

int VideoFeatureExtractor::getMaxGradientV(int radious)
{
    int c = m_integral->width - 1;
    int r, r2;
    int32_t max = -(1 << 30);
    
    for (r = 0; (r2 = r + radious) < m_integral->height; r++) {
        int32_t *row1 = (int32_t *) m_integral->pixels[r];
        int32_t *row2 = (int32_t *) m_integral->pixels[r2];
        
        int grad = row2[c] - row1[c];
        
        if (grad >= max) {
            max = grad;
        }
    }
    
    return max;
}

int VideoFeatureExtractor::addFeature(const Haar::HaarLike1 &haar)
{
    int index = m_haar1.size();
    m_haar1.push_back(haar);
    
    // Update the haar-feature calculation limits
    if (m_col_min < -haar.x_min()) {
        m_col_min = - haar.x_min();
    }
    
    if (m_col_max > width() - haar.x_max()) {
        m_col_max = width() - haar.x_max();
    }
    
    if (m_row_min < -haar.y_min()) {
        m_row_min = - haar.y_min();
    }
    
    if (m_row_max > height() - haar.y_max()) {
        m_row_max = height() - haar.y_max();
    }
    
    
    return index;
}

