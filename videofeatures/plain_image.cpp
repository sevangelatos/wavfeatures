#include "plain_image.h"
#include <stdlib.h>
#include <assert.h>

static void integral_test();

bool PlainImage_RunTests()
{
    integral_test();

    return true;
}


PlainImage PlainImage_Create(int width, int height, int bytes_per_sample, int channels)
{
    PlainImage img;
    int size_needed, i, stride;

    assert(width > 0);
    assert(height > 0);

    // The actual struct
    size_needed = sizeof(*img);

    // Pointers to rows
    size_needed += sizeof(img->pixels[0]) * height;

    // Pixel data
    size_needed += width * height * bytes_per_sample;

    img = (PlainImage) malloc(size_needed);

    img->buffer = NULL;
    img->width = width;
    img->height = height;
    img->bytes_per_sample = bytes_per_sample;
    img->channels = channels;
    img->continuous = true;

    // Init row pointers
    img->pixels = (uint8_t **)(img + 1);
    img->pixels[0] = (uint8_t *) (img->pixels + height);
    stride = width * bytes_per_sample;
    for (i = 1; i < height; ++i) {
        img->pixels[i] = img->pixels[i-1] + stride;
    }

    return img;
}


#ifdef USE_OPENCV_CPP

bool PlainImage_ShallowCopyMat(PlainImage *pdst, cv::Mat &src)
{
    int channels = 0, bytes_per_sample = 0, i;
    PlainImage dst = *pdst;

    switch(src.type()) {

    case CV_8UC1:
        bytes_per_sample = 1;
        channels = 1;
        break;

    case CV_8UC3:
        bytes_per_sample = 3;
        channels = 3;
        break;

    case CV_32SC1:
        bytes_per_sample = 4;
        channels = 1;
        break;

    default:
        assert(!"Unknown Mat type!");
        break;
    }

    if (dst == NULL || dst->height != src.rows) {
        PlainImage_Destroy(dst);
        *pdst = dst = PlainImage_Create(src.cols, src.rows, 0, channels);
    }

    assert(dst->height == src.rows);

    dst->bytes_per_sample = bytes_per_sample;
    dst->width = src.cols;
    dst->channels = channels;
    dst->continuous =  src.isContinuous();

    for (i = 0; i < dst->height; i++) {
        dst->pixels[i] = src.ptr(i);
    }

    return true;
}

bool PlainImage_DeepCopyMat(PlainImage *pdst, cv::Mat &src)
{
    int channels = 0, bytes_per_sample = 0, i;
    PlainImage dst = *pdst;

    switch(src.type()) {

    case CV_8UC1:
        bytes_per_sample = 1;
        channels = 1;
        break;

    case CV_8UC3:
        bytes_per_sample = 3;
        channels = 3;
        break;

    case CV_32SC1:
        bytes_per_sample = 4;
        channels = 1;
        break;

    default:
        assert(!"Unknown Mat type!");
        break;
    }

    if (dst == NULL ||
        dst->height != src.rows ||
        dst->width != src.cols  ||
        dst->channels != channels ||
        dst->bytes_per_sample != bytes_per_sample)
    {
        PlainImage_Destroy(dst);
        *pdst = dst = PlainImage_Create(src.cols, src.rows, bytes_per_sample, channels);
    }

    assert(dst->height == src.rows);

    dst->bytes_per_sample = bytes_per_sample;
    dst->width = src.cols;
    dst->channels = channels;
    dst->continuous =  src.isContinuous();

    for (i = 0; i < dst->height; i++) {
        memcpy(dst->pixels[i], src.ptr(i), dst->width * dst->bytes_per_sample);
    }

    return true;
}


#endif


void PlainImage_Min(PlainImage dest, PlainImage src1, PlainImage src2)
{
    int width, height, r, c;

    assert(src1->width == src2->width);
    assert(src1->height == src2->height);
    assert(src1->width == dest->width);
    assert(src1->height == dest->height);

    width = src1->width;
    height = src1->height;

    if (dest->continuous && src1->continuous && src2->continuous) {
        width = width * height;
        height = 1;
    }

    // Work byte-wise
    width *= src1->bytes_per_sample;

    for (r = 0; r < height; r++) {
        uint8_t *row_dst = dest->pixels[r];
        uint8_t *row1    = src1->pixels[r];
        uint8_t *row2    = src2->pixels[r];

        for (c = 0; c < width; c++) {
            *(row_dst++) = (row1[0] < row2[0]) ? row1[0] : row2[0];
            row1++;
            row2++;
        }
    }

}


void PlainImage_Integral(PlainImage integral, PlainImage input)
{
    int width = integral->width;
    int height = integral->height;
    int r, c;
    int32_t *integral_data = (int32_t *) integral->pixels[0];
    int32_t *it, *up, *left, *ul;
    uint8_t *img;

    assert(input->continuous);
    assert(input->width + 1 == integral->width);
    assert(input->height + 1 == integral->height);
    assert(input->bytes_per_sample == 1);
    assert(input->channels == 1);
    assert(integral->bytes_per_sample == 4);
    assert(integral->channels == 1);
    assert(integral->continuous);

    // Init first row to zero
    memset(integral_data, 0, width * integral->bytes_per_sample);

    // Init first column to zero
    it   = integral_data + width;
    for (r = 1; r < height; r++) {
        *it = 0;
        it += width;
    }

    it   = integral_data + width + 1;
    left = integral_data + width;
    up   = integral_data + 1;
    ul   = integral_data;
    img = &input->pixels[0][0];

    // incrementally build the integral data...
    for (r = 1; r < height; r++) {

        for (c = 1; c < width; c++) {
            *(it++) =  *(img++) + *(up++) - *(ul++) + *(left++);
        }

        // Always skip the first column
        it++; up++; ul++; left++;
    }
}


#define BYTES_PER_SAMPLE 3 // RGB

#define ABSDIFF(X, Y) ( ((X) > (Y)) ? (X)-(Y) : (Y) - (X) )


void PlainImage_Set(PlainImage dst, uint8_t val)
{
    int r;
    int width = dst->width;
    int height = dst->height;

    if (dst->continuous) {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++) {
        memset(dst->pixels[r], val, dst->bytes_per_sample * width);
    }
}

void PlainImage_Copy(PlainImage dst, PlainImage src)
{
    int r;
    int width  = dst->width;
    int height = dst->height;

    assert(dst->height == src->height);
    assert(dst->width == src->width);
    assert(dst->bytes_per_sample == src->bytes_per_sample);
    assert(dst->channels == src->channels);

    if (dst->continuous && src->continuous) {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++) {
        memcpy(dst->pixels[r], src->pixels[r], dst->bytes_per_sample * width);
    }
}



void PlainImage_AbsGrayDiff(PlainImage dst, PlainImage a, PlainImage b)
{
    int r, c, width, height;
    uint8_t *ptr_a, *ptr_b, *ptr_dst;

    width  = dst->width;
    height = dst->height;

    assert(a->width == width);
    assert(a->height == height);
    assert(b->width == width);
    assert(b->height == height);
    assert(a->bytes_per_sample == BYTES_PER_SAMPLE && b->bytes_per_sample == BYTES_PER_SAMPLE);
    assert(a->channels == BYTES_PER_SAMPLE && b->channels == BYTES_PER_SAMPLE);
    assert(dst->channels == 1 && dst->bytes_per_sample == 1);

    // If this is the case, treat all files as a continuous buffer
    if (a->continuous && b->continuous && dst->continuous) {
        width = width * height;
        height = 1;
    }

    for (r = 0; r < height; r++) {

        ptr_a = a->pixels[r];
        ptr_b = b->pixels[r];
        ptr_dst = dst->pixels[r];

        for (c = 0; c < width; c++) {
            int d1 = ABSDIFF(ptr_a[0], ptr_b[0]);
            int d2 = ABSDIFF(ptr_a[1], ptr_b[1]);
            int d3 = ABSDIFF(ptr_a[2], ptr_b[2]);
            int total_delta = (d1 + d2 + d3) >> 1;
            *(ptr_dst++) = MIN(total_delta, 255);
            ptr_a += BYTES_PER_SAMPLE;
            ptr_b += BYTES_PER_SAMPLE;
        }
    }


}



void PlainImage_Destroy(PlainImage img)
{
    if (img && img->buffer) {
        free(img->buffer);
    }

    free(img);
}


static void integral_test()
{
    int r, c;
    PlainImage src = PlainImage_Create(4, 3, 1, 1);
    PlainImage integral = PlainImage_Create(src->width + 1, src->height + 1, 4, 1);

    int32_t result[4][5] = { {0, 0,   0,  0, 0},
                             {0, 0,   1,  3, 6},
                             {0, 10, 22, 36, 52},
                             {0, 30, 63, 99, 138}
                           };

    // Construct a synthetic table
    for (r = 0; r < src->height; r++) {
        for (c = 0; c < src->width; c++) {
            src->pixels[r][c] = 10 * r + c;
        }
    }

    PlainImage_Integral(integral, src);

    for (r = 0; r < integral->height; r++) {
        for (c = 0; c < integral->width; c++) {
            int *integ = (int *) integral->pixels[0];
            int sum = integ[integral->width * r + c];
            assert(sum == result[r][c]);
        }
    }

    PlainImage_Destroy(integral);
    PlainImage_Destroy(src);
}

