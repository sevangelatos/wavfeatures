#ifndef HAAR_H
#define HAAR_H

#include <stdint.h>
#include "plain_image.h"

namespace Haar {

class HaarLike1 {
    int m_x;
    int m_y;
    int m_width;
    int m_height;
    int m_width_a;
    int m_height_a;
    int m_x_a;
    int m_y_a;
    
public:
    /**
     * @brief Constructor
     *
     * @param width   Width of the big haar area
     * @param height  Height of the big haar area
     * @param width_a  Width of the inner haar area
     * @param height_a  Height of the inner haar area
     * @param x_a  Horizontal offset of the inner area within the big area
     * @param y_a  Vertical offset of the inner area within the big area
     * @param offset_x  Vertical offset to apply to the whole haar area when evaluating the feature
     * @param offset_y  Horizontal offset to apply to the whole haar area when evaluating the feature
     */
    HaarLike1(int width, int height, int width_a, int height_a, int x_a, int y_a, int offset_x = 0, int offset_y = 0)
    {
        // Both areas are non-zero
        assert(width_a > 0 && height_a > 0);
        assert(width > 0 && height > 0);
        
        // Area A is within the overall area
        assert(x_a >= 0);
        assert(y_a >= 0);
        assert(x_a + width_a <= width);
        assert(y_a + height_a <= height);
        
        m_x = offset_x - (width / 2);
        m_y = offset_y - (height / 2);
        m_width = width;
        m_height = height;
        
        m_x_a = m_x + x_a;
        m_y_a = m_y + y_a;
        m_width_a = width_a;
        m_height_a = height_a;
    }

    int evaluate(PlainImage integral, int x, int y)
    {
        int x_a = x + m_x_a;
        int y_a = y + m_y_a;
        int sum, sum_a;
        
        x += m_x;
        y += m_y;
        
        assert(x >= 0);
        assert(y >= 0);
        assert(x + m_width <= integral->width);
        assert(y + m_height <= integral->height);
        
        sum = area_sum(integral, x, y, x + m_width, y + m_height);
        sum_a = area_sum(integral, x_a, y_a, x_a + m_width_a, y_a + m_height_a);
        
        return sum - (sum_a << 1);
    }
    
    // The limits of the Haar feature area, if we try to eval the haar in 0,0
    int x_min() const { return  m_x; }
    int x_max() const { return  m_x + m_width; }
    int y_min() const { return  m_y; }
    int y_max() const { return  m_y + m_height; }

protected:

    /**
     * Compute the sum over an area of an image
     * using an integral image.
     *
     * @param integralImage The integral image buffer
     * @param row_lo   Offset (NOT row index) of the data of the row with the lowest index
     * @param row_hi   Offset (NOT row index) of the data of the row with the highest index
     * @param col_lo   Low column index
     * @param col_hi   High column index
     */
    inline int area_sum (PlainImage integral, int x1, int y1, int x2, int y2)
    {
        int32_t * r_lo = (int32_t *) integral->pixels[y1];
        int32_t * r_hi = (int32_t *) integral->pixels[y2];
        return r_hi[x2] - r_hi[x1] + r_lo[x1] - r_lo[x2];
    }
};



} // Namespace

#endif // HAAR_H
