#ifndef VIDEOFEATUREEXTRACTOR_H
#define VIDEOFEATUREEXTRACTOR_H

#include "plain_image.h"
#include <algorithm>
#include <cstdio>
#include <vector>
#include "haar.h"

class VideoFeatureExtractor
{
    PlainImage m_rgb;             // Current RGB frame
    PlainImage m_rgb_upcoming;    // Upcoming RGB frame
    PlainImage m_expired;         // Expired RGB frame
    PlainImage m_delta1;          // Delta frame ABS(upcoming - current)
    PlainImage m_delta2;          // Delta frame ABS(current - previous)
    PlainImage m_delta;           // Delta frame INTERSECTION(delta1, delta2)
    PlainImage m_integral;        // Integral image of m_delta

    int64_t m_timestamp;          // Timestamp for m_rgb, m_delta and m_integral
    int64_t m_timestamp_upcoming; // Timestamp of m_rgb_upcoming
    int     m_counter;
    int     m_row_min;
    int     m_row_max;
    int     m_col_min;
    int     m_col_max;
    
    std::vector<Haar::HaarLike1> m_haar1; // Haar1 style features

public:
    VideoFeatureExtractor(int width, int height);

    ~VideoFeatureExtractor();

    void update(PlainImage rgb_new, int64_t timestamp_new);
    
    void reset();
    
    int addFeature(const Haar::HaarLike1 &haar);

    int getMaxGradientH(int radious);
    int getMaxGradientV(int radious);

    PlainImage frame() { return m_rgb; }

    PlainImage &delta() { return m_delta; }

    PlainImage &expired() { return m_expired; }

    PlainImage &integral() { return m_integral; }

    int64_t    timestamp() {return m_timestamp; }
    
    int32_t    haar1(int feat_id, int r, int c)
    {
        return m_haar1[feat_id].evaluate(m_integral, c, r);
    }
    
    int width() const { return m_rgb->width; }
    int height() const { return m_rgb->height; }
    int min_row() const { return m_row_min; }
    int min_col() const { return m_col_min; }
    int max_row() const { return m_row_max; }
    int max_col() const { return m_col_max; }
};

#endif // VIDEOFEATUREEXTRACTOR_H
