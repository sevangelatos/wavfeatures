/*
 *  dsp_linux.c
 *  
 *  Linux specific implementations of DSP functions.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fftw3.h>

#include "dsp_utils.h"
#include "error_utils.h"


struct FFTReal_s {
    int          len;
    double       *input;
    fftw_complex *output;
    fftw_plan     plan;
};


FFTReal DspUtils_CreateFFTReal (int len)
{
    FFTReal fft;
    int output_len = (len / 2) + 1; // Real_fft(): N (reals) -> 1 + N/2 (Complex)
    
    assert (POW_OF_2(len));
    
    FAILIF((fft = calloc(1, sizeof(*fft))) == NULL);
    
    fft->len = len;
    
    FAILIF((fft->input  = fftw_malloc(sizeof(double) * len)) == NULL);
    FAILIF((fft->output  = fftw_malloc(sizeof(fftw_complex) * output_len)) == NULL);
    
    fft->plan = fftw_plan_dft_r2c_1d(len, fft->input, fft->output, FFTW_ESTIMATE);
    FAILIF(NULL == fft->plan);
    
    return fft;
fail:
    DspUtils_DestroyFFTReal (fft);
    return NULL;
}


void DspUtils_DestroyFFTReal (FFTReal fft)
{
    if (fft == NULL) {
        return;
    }
    
    if (fft->plan) {
        fftw_destroy_plan(fft->plan);
    }
    
    if (fft->input) {
        fftw_free(fft->input);
    }
    
    if (fft->output) {
        fftw_free(fft->output);
    }
    
    free (fft);
}


/*
 * Output array packed like in:
 *
 * http://developer.apple.com/library/ios/documentation/Performance/Conceptual/
 * vDSP_Programming_Guide/UsingFourierTransforms/UsingFourierTransforms.html#/
 * /apple_ref/doc/uid/TP40005147-CH202-15398
 */
void DspUtils_FftReal(FFTReal fft, float *input, float *output, int len)
{
    fftw_complex *offt;
    double       *ifft;
    int i, j;
    
    assert(fft);
    assert(fft->len == len);
    assert(len >= 2);
    
    ifft = fft->input;
    offt = fft->output;
    
    // Copy input data to the buffer
    for (i=0; i < len; i++) {
        ifft[i] = input[i];
    }
    
    // Execute the FFT
    fftw_execute(fft->plan);
    
#if DSP_UTILS_DEBUG > 0
    // The FFT of a sequence of N reals, results in a
    // Sequence of 1+N/2 Imaginary numbers. If N is even,
    // the Img(DC) = 0 and Img(Nyquist) = 0. (Hermitian symmetry)
    // So we discard these when we store the results in the output..
    
    // Display the results
    // Real_fft(): N (reals) -> 1 + N/2 (Complex)
    for (i=0; i < (len / 2) + 1; i++) {
        printf("(%.2lf, %.2lf) ", offt[i][0], offt[i][1]);
    }
    printf("\n");
#endif
    
    output[0] = offt[0][0];     // DC component (only real part. Img(DC) is always 0)
    output[1] = offt[len/2][0]; // Nyquist component (only real part. Img(Nyquist) is always 0)
    
    for (i=1, j=2; i < len / 2; i++, j+=2) {
        output[j]     = offt[i][0]; // Real part
        output[j + 1] = offt[i][1]; // Img part
    }
}

double DspUtils_GetFftRealAmplification(FFTReal fft)
{
    return 1.0 * fft->len;
}

void Dft_Fin()
{
    fftw_cleanup();    
}


