/*
 *  attackdetector.h
 *
 *  Detect the attack of a new sound by analyzing the
 *  spikes in each octave of the spectrum.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef ATTACKDETECTOR_H
#define ATTACKDETECTOR_H

/**
 * The attack detector handle.
 */
typedef struct AttackDetector_s *AttackDetector;

/**
 * Create an attack detector object.
 */
AttackDetector AttackDetector_Create(int window_size, int psd_len);

/**
 * Destroy an attack detector object.
 */
void AttackDetector_Destroy(AttackDetector detect);


/**
 * Update the attack detector with a new PSD.
 *
 * @return The power diff of the octave with the maximum diff.
 */
float AttackDetector_Update(AttackDetector detect, float *psd, int psd_len);

#endif // ATTACKDETECTOR_H
