/*
 *  dsp_utils.c
 *  
 *  DSP related APIs
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef DSP_UTILS_H
#define DSP_UTILS_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define DSP_UTILS_DEBUG 0

#define PI 3.141592653589793238462643383279502884

#define POW_OF_2(x) ((x) > 0 && 0 == ((x) & ((x)-1)))

#define SQUARE(x) ((x) * (x))

typedef struct FFTReal_s *FFTReal;


// -------------- Module method declarations ---------

/**
 * Initialize the DSP utils library.
 * 
 * Call once, before using any of the functions of this header.
 */
void DspUtils_Init();


/**
 * Finalize the DSP utils library.
 * 
 * Call once, to cleanup before exiting the program.
 */
void DspUtils_Fin();

/**
 * Create a context for FFT of real signals
 *
 * @param len The FFT length. Must be a power of 2.
 * 
 * @return The new FFTReal context of NULL on error
 */
FFTReal DspUtils_CreateFFTReal (int len);


/**
 * Destroy an FFTReal context.
 *
 * @param fft The fft context.
 */
void DspUtils_DestroyFFTReal (FFTReal fft);

/**
 * Perform an FFT transofrmation on a real signal.
 *
 * @param fft    The fft context.
 * @param input  Input signal
 * @param output The output vector
 * @param len    Length of the input/output vectors. Must be equal to the fft context lenght.
 * 
 * @remarks  The output vector is in the following format:
 *  [Dc, Nyquist, Real1, Img1, Real2, Img2, ... Realn, Imgn]
 *  n = len-1
 * 
 * This is the same format described in: 
 * http://developer.apple.com/library/ios/documentation/Performance/Conceptual/vDSP_Programming_Guide/UsingFourierTransforms/UsingFourierTransforms.html#//apple_ref/doc/uid/TP40005147-CH202-15398
 * 
 * The output is amplified by an implementation dependent constant factor.
 * Use DspUtils_GetFftRealAmplification to get this factor and multiply each number by 1/factor
 * if you need normalized output.
 */
void DspUtils_FftReal(FFTReal fft, float *input, float *output, int len);


/**
 * Get the amplification factor of the FFT implamentation.
 *
 * @param fft    The fft context.
 * @return The amplification factor
 */
double DspUtils_GetFftRealAmplification(FFTReal fft);

/**
 * Get the frequency of each FFT bin in Hz.
 *
 * @param fft_len       The length of the FFT transform.
 * @param sampling_rate The signal's sampling rate (Hz).
 * @param bin           The FFT bin 0(DC)...FFT_LEN/2 (Nyquist)
 * @return The bin's frequency in HZ
 */
float DspUtils_FftRealBin2Hz(int fft_len, int sampling_rate, int bin);


/**
 * Get FFT bin closest to a frequency in Hz.
 *
 * @param fft_len       The length of the FFT transform.
 * @param sampling_rate The signal's sampling rate (Hz).
 * @param freq          A frequency in Hz <= sampling_rate/2
 * @return The FFT bin 0(DC)...FFT_LEN/2 (Nyquist)
 */
int DspUtils_FftRealHz2Bin(int fft_len, int sampling_rate, float freq);

/**
 * Allocate a vector of reals.
 *
 * @param len The lenght of the vector.
 * @return A vecrot of reals.
 */
float *DspUtils_CreateVector(int len);


/**
 * Return a vector of reals, containing a Hamming window function.
 *
 * @param len The lenght of the vector.
 * @return A hamming windowing function.
 */
float *DspUtils_WindowHamming(int len);


/**
 * Return a vector of reals, containing a Hann window function.
 *
 * @param len The lenght of the vector.
 * @return A Hann windowing function.
 */
float *DspUtils_WindowHann(int len);


/**
 * Performs an entry-wise multiplication of two signals
 * 
 * result[n] = a[n] * b[n]
 *
 * @param result The vector to store the result
 * @param a      The first input vector
 * @param b      The second input vector
 * @param len    The lenght of the vectors
 */
void DspUtils_VectorEntrywiseProduct(float *result, float *a, float *b, int len);

/**
 * Calculates the first derivative of a signal.
 *
 * The derivative is padded by repeating the last value, to 
 * have the same length as the signal.
 * 
 * derivative[n] = signal[n+1] - signal[n]
 *
 * @param derivative The approximated derivative
 * @param input      The input signal
 * @param len        The lenght of the vectors
 */
void DspUtils_Derivative(float *derivative, float *input, int len);

/**
 * Converts a linear scale signal, to a decibel scale signal
 * 
 * decibel[n] = 10 * log10(input[n] / level_db0)
 *
 * @param decibel The vector to store the result
 * @param input   The first input vector
 * @param level_db0   The reference 0 dB level
 * @param len     The lenght of the vectors
 */
void DspUtils_VectorLinearToDecibel(float *decibel, float *input, float level_db0, int len);


/**
 * Add a scalar to every element of a vector.
 *
 * result[n] = a[n] * scalar
 *
 * @param result The vector to store the result
 * @param input  The input vector
 * @param len    The lenght of the vectors
 * @param factor The scalar factor
 */
void DspUtils_VectorAddScalar(float *result, float *input, int len, float add);


/**
 * Scale a vector by mutiplying every element with a scalar.
 *
 * result[n] = a[n] * scalar
 * 
 * @param result The vector to store the result
 * @param a      The input vector
 * @param factor The scalar factor
 * @param len    The lenght of the vectors
 */
void DspUtils_VectorScale(float *result, float *a, float factor, int len);


/**
 * Clip (limit) the values of a vector.
 *
 * @param output  The clipped output vector
 * @param input   A vector of floats
 * @param len     The length of input/output
 * @param min     The minimum clipping point
 * @param max     The maximum clipping point
 * @param newline If true, also append a newline after the vector
 */
void DspUtils_VectorClip(float *output, float *input, int len, float min, float max);

/**
 * Compute the sum of the values of a vector.
 *
 * @param input   A vector of floats
 * @param len     The length of input
 * @return        The sum of the values in input
 */
float DspUtils_VectorSum(float *input, int len);

/**
 * Convert int16 little endian data to floats.
 *
 * result must have enough space for floor(len/stride) elements
 *
 * @param result  A vector to store the resulting float vector.
 * @param input   Uint32_t input data
 * @param stride  Stride when reading the input data
 * @param len     The length of input
 * @param newline If true, also append a newline after the vector
 */
void DspUtils_Int16ToFloat(float *result, int16_t *input, int stride, int len);


/**
 * Print a vector to a file stream.
 *
 * @param fp    A file stream
 * @param vect  A vector of reals
 * @param len   The length of vect
 * @param newline If true, also append a newline after the vector
 */
void DspUtils_VectorPrint(FILE * fp, float * vect, uint32_t len, bool newline);


/**
 * Obtain the Power Spectral Density from a real fourier 
 * transform of a signal.
 * 
 * The format of fourier should be:
 * [DC, Nyquist, Rel(1), img(1), Rel(2), Img(2), ...]
 * 
 * The psd will be in the format:
 * [|DC|^2, |M1|^2, |M2|^2, ..., |Nyquist|^2]
 *
 * @param psd         The spectral power distribution
 * @param psd_len     The psd buffer length (must be 1 + fourier_len/2)
 * @param fourier     The fourier transform of a real signal
 * @param fourier_len The length of the fourier
 */
void DspUtils_FourierToPsd(float *psd, int psd_len, float *fourier, int fourier_len);


/**
 * Calculate the total energy of a signal from its PSD
 *
 * @param psd         The spectral power distribution (linear, NOT dB)
 * @param psd_len     The length of psd
 * @return            The total energy
 */
float DspUtils_EnergyFromPSD(float *psd, int psd_len);


/**
 * Calculate the energy of a Hz frequency range from its PSD
 *
 * @param psd         The spectral power distribution (linear, NOT dB)
 * @param psd_len     The length of psd
 * @param sample_rate The sampling rate in Hz
 * @param low         The lower point of the frequency range
 * @param high        The upper point of the frequency range
 *
 * @return The energy in the frequency band
 */
float DspUtils_BandEnergyHz(float *psd, int psd_len, int sample_rate, float low, float high);


/**
 * Calculate the energy of a frequency range expressed in PSD bins from a PSD
 *
 * @param psd         The spectral power distribution (linear, NOT dB)
 * @param psd_len     The length of psd
 * @param low         The lower point of the frequency range (in bins (0 - psd_len))
 * @param high        The upper point of the frequency range (in bins (0 - psd_len))
 *
 * @return The energy in the frequency band
 */
float DspUtils_BandEnergyBins(float *psd, int psd_len, float low, float high);

/**
 * Resample the psd to a logarithmic frequency scale
 */
void DspUtils_Psd2LogScale(float *log_spectrum, int spectrum_len, float *psd, int psd_len, int sample_rate);

/**
 * Calculate the power of a signal
 *
 * SUM(signal[i] ^2)/N
 * 
 * @param signal A real signal
 * @param len    The length of signal
 * @return The signal power
 */
float DspUtils_Power(float *signal, int len);


/**
 * Calculate the % rate of zero crossings
 *
 * @param input  A real signal
 * @param len    The length of input
 * @return The zero crossing rate of the input signal.
 */
float DspUtils_ZeroCrossingRate(float *input, int len);


/**
 * Calculate signal RMS
 *
 * @param signal A real signal
 * @param len    The length of signal
 * @return The signal RMS
 */
float DspUtils_RMS(float *signal, int len);

/**
 * Calculate log2(N)
 * 
 * Works only when N is a power of 2
 *
 * @param n A power of 2
 * @return log2(n)
 */
int DspUtils_Log2(uint32_t n);

/**
 * Print a psd as Frequency Power pairs
 *
 *
 * @param file        A writable file handle
 * @param psd         The power spectral density in either linear or dB scale.
 * @param psd_len     The psd length (Normally, (fft_len/2 + 1)
 * @param sample_rate The original signal sampling rate
 * @return log2(n)
 */
void DspUtils_PrintPsd(FILE *file, float *psd, int psd_len, int sample_rate);

/**
 * Convert a quantity from linear to decibels.
 *
 * @param a        A quantity to convert to decibel
 * @return 10 * log10(a)
 */
double DspUtils_Decibel(double a);

/**
 * Convert a quantity from decibel to linear.
 *
 *
 * @param db        A quantity to convert from decibel
 * @return 10^(db/10.0)
 */
double DspUtils_Decibel2Linear(double db);

#endif // DSP_UTILS_H


