/*
 *  feature_extractor.c
 *
 *  Extract feature vectors from audio chunks.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include "feature_extractor.h"

#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include "denoiser.h"
#include "wav_reader.h"
#include "error_utils.h"
#include "dsp_utils.h"
#include "attackdetector.h"
#include "decaydetector.h"
#include "statistics.h"
#include "spectral_features.h"

#define MAX_FEATURES 32
#define INITIAL_NOISE_LEVEL_DB -40
#define DECAY_LENGTH 4
#define DECAY_DB     5.0

static void calc_cross_window_features(FeatureExtractor fe, FeatureVector target, int latest_sample_offset);
static float measure_impulsivity(float *audio, int audio_len, int window);
static void feature_extractor_history_commit(FeatureExtractor fe);
static FeatureVector feature_extractor_history_pop(FeatureExtractor fe);
static FeatureVector feature_extractor_history_get_free(FeatureExtractor fe);
static FeatureVector feature_extractor_history_get_last(FeatureExtractor fe);

struct FeatureExtractor_s
{
    FFTReal fft;
    Denoiser denoiser;
    AttackDetector attack;
    DecayDetector  decay;

    int sample_rate;
    int     psd_len;
    int     fft_len;
    float *fourier;
    float *psd;
    float *window;
    double normal_fact;

    int    vector_first;
    int    vector_count;
    int    history;

    struct FeatureVector_s *feature_vectors;
    float  *feature_data; //Actual data of the feature vectors
};


FeatureExtractor FeatureExtractor_Create(int sample_rate, int fft_len, int history)
{
    FeatureExtractor fe = NULL;
    int i, psd_len = 1 + fft_len/2;

    FAILIF((fe = calloc(1, sizeof(*fe))) == NULL);

    FAILIF(!(fe->feature_vectors = calloc(1, sizeof(fe->feature_vectors[0]) * history)));

    FAILIF(!(fe->feature_data = calloc(1, sizeof(float) * history * MAX_FEATURES)));

    DspUtils_Init();

    fe->fft_len = fft_len;
    fe->psd_len = psd_len;
    fe->sample_rate = sample_rate;
    fe->fft = DspUtils_CreateFFTReal(fft_len);
    fe->denoiser = Denoiser_Create(psd_len, DspUtils_Decibel2Linear(INITIAL_NOISE_LEVEL_DB));
    fe->fourier = DspUtils_CreateVector(fft_len);
    fe->psd     = DspUtils_CreateVector(psd_len);
    fe->window  = DspUtils_WindowHamming(fft_len);
    fe->normal_fact = 1.0 / DspUtils_GetFftRealAmplification(fe->fft);
    fe->attack  = AttackDetector_Create(5, psd_len);
    fe->decay   = DecayDetector_Create(DECAY_LENGTH, DECAY_DB);
    fe->history = history;

    /* Initialize the pointers of the feature vectors */
    for (i = 0; i < history; i++) {
        fe->feature_vectors[i].values = fe->feature_data + i * MAX_FEATURES;
    }

    return fe;
fail:
    FeatureExtractor_Destroy(fe);
    return NULL;
}

FeatureVector FeatureExtractor_Extract(FeatureExtractor fe, float *audio, int len, int64_t sample_offset)
{
    float power, power_db, denoised_power, attack_score, decay_score;
    float zero_crossing_rate;
    float impulsivity;
    float *features;
    FeatureVector fvector;
    int   fc = 0;  // Features counter
    int   psd_len;
    const float min_energy = 1.0e-10;

    assert(fe);
    assert(audio);
    assert(len == fe->fft_len);

    NOT_USED(sample_offset);

    psd_len = fe->psd_len;

    // Get a free feature vector from the cyclic buffer to fill
    fvector = feature_extractor_history_get_free(fe);
    features = fvector->values;

    zero_crossing_rate = DspUtils_ZeroCrossingRate(audio, len);

    impulsivity = measure_impulsivity(audio, len, len/3);

    // Apply window to data block
    DspUtils_VectorEntrywiseProduct(audio, audio, fe->window, len);

    // Extract the power in this block...
    power = DspUtils_Power(audio, len);
    power_db = DspUtils_Decibel(power + min_energy); // Limit to min energy to avoid infinities

    // Perform an FFT
    DspUtils_FftReal(fe->fft, audio, fe->fourier, len);

    // Normalize the FFT
    DspUtils_VectorScale(fe->fourier, fe->fourier, fe->normal_fact, len);

    // get the PSD from the FFT
    DspUtils_FourierToPsd(fe->psd, psd_len, fe->fourier, len);

    // The attack and decay scores
    attack_score  = DspUtils_Decibel(AttackDetector_Update(fe->attack, fe->psd, psd_len));
    decay_score   = DecayDetector_Update(fe->decay, power_db);

    // Perform noise reduction on the PSD
    Denoiser_Update(fe->denoiser, fe->psd, psd_len, power);
    Denoiser_NoiseReducePsd(fe->denoiser, fe->psd, psd_len);

    // Detect the power level after denoising
    denoised_power = DspUtils_Decibel(DspUtils_EnergyFromPSD(fe->psd, psd_len) + min_energy);

    features[fc++] = power_db;
    features[fc++] = denoised_power;
    features[fc++] = attack_score;
    features[fc++] = decay_score;
    features[fc++] = zero_crossing_rate;
    features[fc++] = impulsivity;

    // Add the spectral features
    fc += SpectralFeatures(features + fc, MAX_FEATURES - fc,
                           fe->psd, psd_len, fe->sample_rate);

    fvector->class_label = 0;
    fvector->count = fc;
    fvector->sample_offset = sample_offset;

    // Commit the new vector
    feature_extractor_history_commit(fe);

    // Our circular buffer is complete...
    if (fe->vector_count >= fe->history) {
        // Get the first vector...
        fvector = feature_extractor_history_pop(fe);

        // Calculate inter-block features (like duration)
        calc_cross_window_features(fe, fvector, sample_offset);
    }
    else {
        fvector = NULL;
    }

    return fvector;
}

FeatureVector FeatureExtractor_GetFinal(FeatureExtractor fe)
{
    FeatureVector fvector, fv_last;
    assert(fe);

    if (fe->vector_count <= 0) {
        return NULL;
    }

    fv_last = feature_extractor_history_get_last(fe);

    // pop the first vector...
    fvector = feature_extractor_history_pop(fe);

    // Calculate inter-block features (like duration)
    calc_cross_window_features(fe, fvector, fv_last->sample_offset);

    return fvector;
}



void FeatureExtractor_Destroy(FeatureExtractor fe)
{
    if (NULL == fe) {
        return;
    }

    if (fe->fft) {
        DspUtils_DestroyFFTReal(fe->fft);
    }

    if (fe->denoiser) {
        Denoiser_Destroy(fe->denoiser);
    }

    free(fe->fourier);
    free(fe->psd);
    free(fe->window);
    free(fe->feature_vectors);
    free(fe->feature_data);

    if (fe->attack) {
        AttackDetector_Destroy(fe->attack);
    }

    if (fe->decay) {
        DecayDetector_Destroy(fe->decay);
    }

    memset(fe, 0, sizeof(*fe));
    free(fe);

    DspUtils_Fin();
}

#define MAX_IMPULSE_WINDOWS 20
static float measure_impulsivity(float *audio, int audio_len, int window)
{
    float variance, mean, max = 0.0;
    int i;
    int num_values = audio_len / window;
    float power_values[MAX_IMPULSE_WINDOWS];

    assert(num_values <= MAX_IMPULSE_WINDOWS);

    // Calculate the power in each window..
    for (i = 0; i < num_values; i++) {
        float power = DspUtils_Power(audio, window);

        max = MAX(max, power);
        power_values[i] = power;

        audio += window;
    }

    // Normalize the power values
    if (max > 0.0) {
        DspUtils_VectorScale(power_values, power_values, 1.0f/max, num_values);
    }

    // Return the stddev in dB
    mean = Statistics_Mean(power_values, num_values);
    variance = Statistics_Variance(power_values, num_values, mean);
    return variance;
}


#define DECAY_FEATURE_INDEX  3
#define POWER_FEATURE_INDEX  0

#define INDEX_MOD (idx, base, mod) ((base + idx + mod) % mod)
static void calc_cross_window_features(FeatureExtractor fe, FeatureVector target, int latest_sample_offset)
{
    int c, i;
    int target_index = target - fe->feature_vectors;
    int start_time = target->sample_offset;
    int end_time = latest_sample_offset;
    int forward_100ms = start_time + (100 * fe->sample_rate) / 1000;
    int forward_50ms = start_time + (50 * fe->sample_rate) / 1000;
    float dbpower_after_100ms = -96.0;
    float dbpower_after_50ms = -96.0;
    float dbpower = target->values[POWER_FEATURE_INDEX];

    bool power_100ms_found = false;
    bool power_50ms_found = false;

    assert(target_index < fe->history);
    assert(target_index >= 0);

    //TODO: Optimize this. We should jump directly to the +50 and +100 ms spot and
    // we should limit history to something meaningfull in order to minimise the loops used
    // for duration..

    i = target_index;
    for (c = 0; c < fe->vector_count; c++) {
        FeatureVector v;
        // Wrap around
        if (++i >= fe->history) {
            i = 0;
        }

        v = &fe->feature_vectors[i];

        if (!power_50ms_found && v->sample_offset >= forward_50ms) {
            dbpower_after_50ms = v->values[POWER_FEATURE_INDEX];
            power_50ms_found = true;
        }

        if (!power_100ms_found && v->sample_offset >= forward_100ms) {
            dbpower_after_100ms = v->values[POWER_FEATURE_INDEX];
            power_100ms_found = true;
        }

        if (v->values[DECAY_FEATURE_INDEX] != 0.0) {
            //end_time = fe->feature_vectors[(i + fe->history - DECAY_LENGTH + 1) % fe->history].sample_offset; //This is where the decay actually started...
            end_time = v->sample_offset;
        }
    }

    target->values[target->count++] = end_time - start_time;
    target->values[target->count++] = 100 * dbpower_after_100ms - 100 * dbpower;
    target->values[target->count++] = 100 * dbpower_after_50ms - 100 * dbpower; // Ratio of power now vs power in 50ms
}

static void feature_extractor_history_commit(FeatureExtractor fe)
{
    fe->vector_count += 1;
    assert(fe->vector_count <= fe->history);
}


static FeatureVector feature_extractor_history_pop(FeatureExtractor fe)
{
    int first = fe->vector_first;
    fe->vector_first = (fe->vector_first + 1) % fe->history;
    fe->vector_count -= 1;
    return &fe->feature_vectors[first];
}

static FeatureVector feature_extractor_history_get_last(FeatureExtractor fe)
{
    int last = (fe->vector_first + fe->vector_count - 1) % fe->history;

    assert(fe->vector_count > 0);

    return &fe->feature_vectors[last];
}


static FeatureVector feature_extractor_history_get_free(FeatureExtractor fe)
{
    assert(fe->vector_count < fe->history);
    assert(fe->vector_first < fe->history);
    return &fe->feature_vectors[(fe->vector_first + fe->vector_count) % fe->history];
}

