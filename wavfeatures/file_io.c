/*
 *  file_io.c
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include "file_io.h"
#include <assert.h>
#include <string.h>

#define HOST_LITTLE_ENDIAN 1
#define HOST_BIG_ENDIAN    2
#define HOST_ENDIANESS  HOST_LITTLE_ENDIAN

void swap_endianess(void *buff, int len)
{
    int i = 0, n = len - 1;
    uint8_t *p = buff;

    len = len / 2;

    while(i < len) {
        uint8_t tmp = p[i];
        p[i++] = p[n];
        p[n--] = tmp;
    }

}

static int io_read(IO io, void *buffer, int len)
{
    int i;
    assert(io);
    assert(io->_file);

    if (buffer != NULL) {
        return fread(buffer, 1, len, io->_file);
    }


    // Just skip bytes..
    for (i = 0; i < len; i++) {
        if (fgetc(io->_file) == EOF){
            break;
        }
    }

    return i;
}


static uint8_t io_read_byte(IO io)
{
    uint8_t byte = 0;

    assert(io->read);

    io->read(io, &byte, sizeof(byte));

    return byte;
}

static uint32_t io_le_dword(IO io)
{
    uint32_t dword = 0;

    assert(io->read);

    io->read(io, &dword, sizeof(dword));

#if HOST_ENDIANESS == HOST_BIG_ENDIAN
    swap_endianess(&dword, sizeof(dword));
#endif

    return dword;
}

static uint16_t io_le_word(IO io)
{
    uint16_t word = 0;

    assert(io->read);

    io->read(io, &word, sizeof(word));

#if HOST_ENDIANESS == HOST_BIG_ENDIAN
    swap_endianess(&word, sizeof(word));
#endif

    return word;
}

char *io_bstring(IO io, char *str, int len)
{
    int r;
    assert(str);
    assert(io && io->read);

    r = io->read(io, str, len);
    assert(r <= len);
    str[r] = '\0';
    return str;
}


static bool io_eof(IO io)
{
    assert(io->_file);
    return feof(io->_file);
}


static void io_close(IO io)
{
    if (io && io->_file) {
        fclose(io->_file);
        io->_file = NULL;
    }

    memset(io, 0, sizeof(*io));
}

static bool io_seek(IO io, off_t offset, int whence)
{
    assert(io && io->_file);
    return 0 == fseeko(io->_file, offset, whence);
}

static int64_t io_tell(IO io)
{
    assert(io && io->_file);
    
    return ftello(io->_file); 
}

bool IO_from_filename(IO io, const char *filename)
{
    assert(filename);
    assert(io);

    io->_file = fopen(filename, "rb");

    if (io->_file == NULL) {
        perror(filename);
        return false;
    }

    io->close    = io_close;
    io->byte     = io_read_byte;
    io->read     = io_read;
    io->eof      = io_eof;
    io->le_dword = io_le_dword;
    io->le_word  = io_le_word;
    io->bstring  = io_bstring;
    io->seek     = io_seek;
    io->tell     = io_tell;

    return true;
}

