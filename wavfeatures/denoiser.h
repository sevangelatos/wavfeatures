/*
 *  denoiser.h
 *
 *  Remove the influence of noise from a PSD.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */
#ifndef DENOISE_H
#define DENOISE_H

#include <stdbool.h>

typedef struct Denoiser_s *Denoiser;

Denoiser Denoiser_Create(int spd_size, float noise_threshold);

void Denoiser_Destroy(Denoiser dnoise);

bool Denoiser_Update(Denoiser dnoise, float *psd, int psd_len, float psd_level);

bool Denoiser_NoiseReducePsd(Denoiser dnoise, float *psd, int psd_size);

#endif // DENOISE_H
