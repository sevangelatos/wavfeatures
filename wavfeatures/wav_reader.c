/*
 *  wav_reader.c
 *
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include "wav_reader.h"
#include "error_utils.h"
#include "file_io.h"

#include <stdlib.h>
#include <string.h>

#define WAV_FORMAT_PCM 0x001

/* Build a RIFF tag */
#define TAG(A, B, C, D) ( (((uint32_t)(A))) | (((uint32_t)(B)) << 8) | (((uint32_t)(C)) << 16) | (((uint32_t)(D)) << 24))

/* Special error return value for RIFF chunk handlers */
#define HANDLER_ERROR UINT32_MAX

struct CuePoint_s {
    CuePoint next;
    uint32_t id;
    uint32_t play_order;
    uint32_t sample_offset;
    uint32_t length;
    char     label[64];
    char     note[64];
    char     text[64];
};

typedef uint32_t (*RiffHandler)(WavReader hWav, uint32_t tag, uint32_t len);


/* --------------- Local function declarations ----------------- */

static uint32_t wav_reader_parse_chunk(WavReader hWav);
static uint32_t riff_handler_skip(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_RIFF(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_fmt(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_data(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_cue(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_list(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_labl(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_ltxt(WavReader hWav, uint32_t tag, uint32_t len);
static uint32_t riff_handler_mark(WavReader hWav, uint32_t tag, uint32_t len);

static struct {
    uint32_t    tag;
    RiffHandler handler;
} chunk_handlers[] = {
    { TAG('R', 'I', 'F', 'F'), riff_handler_RIFF },
    { TAG('f', 'm', 't', ' '), riff_handler_fmt  },
    { TAG('d', 'a', 't', 'a'), riff_handler_data },
    { TAG('c', 'u', 'e', ' '), riff_handler_cue  },
    { TAG('l', 't', 'x', 't'), riff_handler_ltxt },
    { TAG('n', 'o', 't', 'e'), riff_handler_labl },
    { TAG('l', 'a', 'b', 'l'), riff_handler_labl },    
    { TAG('l', 'i', 's', 't'), riff_handler_list },
    { TAG('L', 'I', 'S', 'T'), riff_handler_list },
    { TAG('M', 'A', 'R', 'K'), riff_handler_mark }
};

/* --------------- Public function definitions ----------------- */

WavReader WavReader_OpenFile(const char* filename)
{
    WavReader hWav;

    assert(filename);

    FAILIF ((hWav = (WavReader) calloc(1, sizeof(*hWav))) == NULL);

    FAILIF (!IO_from_filename(&hWav->io, filename));

    FAILIF (!wav_reader_parse_chunk(hWav));

    return hWav;

fail:
    return NULL;
}


CuePoint WavReader_GetNextCuePoint(WavReader hWav, CuePoint point)
{
    assert(hWav);
    
    if (point == NULL) {
        return hWav->cues;
    }
    else {
        return point->next;
    }
}

uint32_t WavReader_CuePointGetId(CuePoint point)
{
    assert(point);
    return point->id;
}

uint32_t WavReader_CuePointGetPosition(CuePoint point)
{
    assert(point);
    return point->sample_offset;
}

uint32_t WavReader_CuePointGetLength(CuePoint point)
{
    assert(point);
    return point->length;
}


const char * WavReader_CuePointGetNote(CuePoint point)
{
    assert(point);
    return point->note;
}

const char * WavReader_CuePointGetLabel(CuePoint point)
{
    assert(point);
    return point->label;
}

const char * WavReader_CuePointGetText(CuePoint point)
{
    assert(point);
    return point->text;
}

int WavReader_ReadAudio(WavReader hWav, uint32_t start_sample, void *buffer, uint32_t size)
{
    IO io = &hWav->io;
    uint64_t offset = hWav->data_offset + start_sample * hWav->block_align; 
        
    if (!io->seek(io, offset, SEEK_SET)) {
        return -1;
    }
    
    // Don't exceed the number of total bytes
    size = MIN(size, hWav->audiodata_size);
    
    
    // Read whole number of samples
    size -= size % hWav->block_align;
    
    return io->read(io, buffer, size);
}


void WavReader_Close(WavReader hWav)
{
    CuePoint point;
    IO io = &hWav->io;

    if (!hWav) { return; }
    
    // Free the cue points list
    point = hWav->cues;
    while (point) {
        CuePoint next = point->next;
        free(point);
        point = next;
    }

    if (io->close) {
        io->close(io);
    }

    free(hWav);
}


static uint32_t wav_reader_parse_chunk(WavReader hWav)
{
    uint32_t chunk_tag;
    uint32_t chunk_len;
    uint32_t ret;
    IO io = &hWav->io;
    RiffHandler handler = riff_handler_skip;
    int num_handlers = sizeof(chunk_handlers) / sizeof(chunk_handlers[0]);
    int i;

    chunk_tag = io->le_dword(io);
    chunk_len = io->le_dword(io);

    for (i = 0; i < num_handlers; i++) {
        if (chunk_handlers[i].tag == chunk_tag) {
            handler = chunk_handlers[i].handler;
            break;
        }
    }

    if ((ret = handler(hWav, chunk_tag, chunk_len)) == HANDLER_ERROR) {
        return HANDLER_ERROR;
    }
    
    /* Skip one byte to always align chunks to odd bytes */
    if (chunk_len & 0x1) {
        io->read(io, NULL, 1);
    }

    return sizeof(chunk_tag) + sizeof(chunk_len) + ret;
}


static uint32_t wav_reader_parse_consecutive_chunks(WavReader hWav, uint32_t total_size)
{
    uint32_t remaining = total_size;
    IO io = &hWav->io;

    while (remaining >= 8) {
        uint32_t parsed = wav_reader_parse_chunk(hWav);

        if (parsed == HANDLER_ERROR ||
            parsed > remaining ||
            io->eof(io))
        {
            return HANDLER_ERROR;
        }

        remaining -= parsed;
    }

    return total_size;
}


static uint32_t riff_handler_RIFF(WavReader hWav, uint32_t tag, uint32_t len)
{
    char file_type[5];
    IO io = &hWav->io;

    NOT_USED(tag);

    io->bstring(io, file_type, 4);
    if ( strcmp(file_type, "WAVE") != 0 ) {
        fprintf(stderr, "Only WAVE format supported\n");
        return HANDLER_ERROR;
    }

    return wav_reader_parse_consecutive_chunks(hWav, len - 4);
}


static uint32_t riff_handler_data(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    
    NOT_USED(tag);
    /* Make a note of the position and length of the audio data */
    hWav->data_offset = io->tell(io);
    if (hWav->data_offset < 0) {
        return HANDLER_ERROR;
    }
    hWav->audiodata_size = len;
    
    
    /* Skip the data */
    io->seek(io, len, SEEK_CUR);
    
    return len;
}

static CuePoint cue_get_by_id(WavReader hWav, uint32_t id, bool create)
{
    CuePoint point = hWav->cues;
    
    // Take care of creating the first node
    if (point == NULL && create) {
        point = calloc(1, sizeof(*point));
        point->id = id;
        hWav->cues = point;
    }
    
    // Find or create an appropriate node
    while (point && point->id != id) {
        if (point->next == NULL && create) {
            point->next = calloc(1, sizeof(*point));
            point->next->id = id;
        }
        
        point = point->next;
    }
    
    assert(point || !create);
    return point;
}

static uint32_t riff_handler_cue(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    uint32_t i, num_cue_points, used = 0;
    
    NOT_USED(tag);
    
    num_cue_points = io->le_dword(io);
    used += sizeof(num_cue_points);
    
    for (i = 0; i < num_cue_points && used < len && !io->eof(io); i++) {
        uint32_t id = io->le_dword(io);
        CuePoint point = cue_get_by_id(hWav, id, true);
        
        point->play_order = io->le_dword(io);
        io->le_dword(io); // skip data chunk id
        io->le_dword(io); // skip chunk start
        io->le_dword(io); // skip block start
        point->sample_offset = io->le_dword(io);

        used += 6 * sizeof(uint32_t);
    }
    
    // Skip any remaining bytes
    if (len > used) {
        io->read(io, NULL, len - used);        
    }    
    
    return len;
}

static uint32_t riff_handler_list(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    uint32_t consumed = len;
    uint32_t type_id = io->le_dword(io);

    NOT_USED(tag);
    
    if (type_id == TAG('a', 'd', 't', 'l')) {
        consumed = wav_reader_parse_consecutive_chunks(hWav, len - sizeof(type_id));

        if (consumed == HANDLER_ERROR || consumed + sizeof(type_id) > len) {
            return HANDLER_ERROR;
        }
        
        consumed += sizeof(type_id);
    }
    
    return consumed;
}


static uint32_t riff_handler_fmt(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;

    NOT_USED(tag);

    hWav->format_tag    = io->le_word(io);
    hWav->channels      = io->le_word(io);
    hWav->sampling_rate = io->le_dword(io);
    hWav->bytes_per_sec = io->le_dword(io);
    hWav->block_align   = io->le_word(io);
    
    if (hWav->channels) {
        hWav->bits_per_sample = (8 * hWav->block_align) / hWav->channels;
    }

    if (hWav->format_tag != WAV_FORMAT_PCM) {
        fprintf(stderr, "Only PCM format supported\n");
        return HANDLER_ERROR;
    }

    // Skip the rest of the tag
    io->read(io, NULL, len - 14);
    return len;
}

/* 
 * Read a note or label chunk, containing a note or label for a cue point.
 * 
 * It truncates the note/label size to at most sizeof(label[]/note[]) chars.
 */
static uint32_t riff_handler_labl(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    uint32_t read = 0;
    uint32_t cue_id;
    CuePoint point;
    
    cue_id = io->le_dword(io);
    point = cue_get_by_id(hWav, cue_id, true);
    
    if (tag == TAG('l', 'a', 'b', 'l')) {
        read = MIN(sizeof(point->label) - 1, len - sizeof(cue_id));
        io->read(io, point->label, read);
        point->label[read] = '\0';
    }
    else if (tag == TAG('n', 'o', 't', 'e')) {
        read = MIN(sizeof(point->note) - 1, len - sizeof(cue_id));
        io->read(io, point->note, read);
        point->note[read] = '\0';
    }
    else {
        return HANDLER_ERROR;
    }
    
    read += sizeof(cue_id);
    
    // Skip remaining data
    if (read < len) {
        io->read(io, NULL, len - read);
    }
    
    return len;
}


static uint32_t riff_handler_ltxt(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    uint32_t cue_id;
    CuePoint point;
    uint32_t read = 0;

    NOT_USED(tag);
    
    cue_id = io->le_dword(io);
    point = cue_get_by_id(hWav, cue_id, true);
    
    point->length = io->le_dword(io);
    io->le_dword(io); // Skip Purpose
    io->le_word(io); // Skip Country
    io->le_word(io); // Skip Language
    io->le_word(io); // Skip Dialect
    io->le_word(io); // Skip CodePage
    
    read += sizeof(uint32_t) * 3 + sizeof(uint16_t) * 4;
    
    if (read < len) {
        int to_read = MIN(sizeof(point->text) - 1, len - read);
        io->read(io, point->note, to_read);
        point->note[to_read] = '\0';
        read += to_read;
    }
    
    // Skip remaining data
    if (read < len) {
        io->read(io, NULL, len - read);
    }
    
    return len;
}

/*
 * This is a proprietary chunk keeping the markers for Sound Studio by
 * "Felt Tip Software". I have hopefully figured out the format correctly.
 */
static uint32_t riff_handler_mark(WavReader hWav, uint32_t tag, uint32_t len)
{
    IO io = &hWav->io;
    uint16_t num_markers;
    uint32_t i, consumed = 0;

    NOT_USED(tag);
    
    num_markers = io->le_word(io);
    consumed += 2;
    
    for (i = 0; i < num_markers && consumed + 8 <= len; i++) {
        uint16_t to_read, label_len;
        CuePoint point = cue_get_by_id(hWav, io->le_word(io), true);
        point->sample_offset = io->le_dword(io);
        
        label_len = io->byte(io);
        to_read = MIN(sizeof(point->label) - 1, label_len);
        consumed += 7;
        
        // Account for padding...
        label_len |= 0x1;
        
        // Carefull not to read beyond the chunk...
        if (consumed + label_len > len) {
            return HANDLER_ERROR;
        }
        
        io->read(io, point->label, to_read);
        point->label[to_read] = '\0';
        
        if (to_read < label_len) {
            io->read(io, NULL, label_len - to_read);
        }
        consumed += label_len;
    }
    
    return len;
}


static uint32_t riff_handler_skip(WavReader hWav, uint32_t tag, uint32_t len)
{
    char str_tag[5];

    NOT_USED(hWav);

    str_tag[0] = tag;
    str_tag[1] = tag >> 8;
    str_tag[2] = tag >> 16;
    str_tag[3] = tag >> 24;
    str_tag[4] = '\0';

    printf("Skipping tag: %s (%u)\n", str_tag, len);
    return len;
}


