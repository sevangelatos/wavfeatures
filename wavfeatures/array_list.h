// Ensure we are only included once
#ifndef __ARRAY_LIST_H
#define __ARRAY_LIST_H

#ifdef __cplusplus
 extern "C" {
#endif 

#include <stdbool.h>
 #include <stdint.h>

// Typedefines and defines -----------------------------------------------------
typedef struct ArrayList_s        *ArrayList;

typedef int (*pfnCompar_t)(const void *, const void *);
// Global variables ------------------------------------------------------------


// Procedure prototypes ---------------------------------------------------------

/**
* ArrayList_Create
*
* Creates a list object
*
* @param phList   On return it will contain the list handle
*
* @return true if successful, false otherwise.
*
*/
bool ArrayList_Create(ArrayList *phList);

/**
* ArrayList_Destroy
*
* Destroys a list object
*
* @param hList    The list object to destroy
*
* @return 
*
*/
void ArrayList_Destroy(ArrayList hList);


/**
* ArrayList_GetSize
*
* Gets the number of list elements
*
* @param hList  The list object
*
* @return the number of list elements
*
*/
uint32_t ArrayList_GetSize(ArrayList hList);

/**
* ArrayList_LinkFirst
*
* Adds an element at the beginning of the list
*
* @param hList   The list object
* @param pvData  The data to add
*
* @return true on success, false otherwise
*
*/
bool ArrayList_LinkFirst(ArrayList hList, void *pvData);


/**
* ArrayList_LinkLast
*
* Adds an element at the end of the list
*
* @param hList   The list object
* @param pvData  The data to add
*
* @return true on success, false otherwise
*
*/
bool ArrayList_LinkLast(ArrayList hList, void *pvData);

/**
* ArrayList_UnlinkFirst
*
* Removes an element from the beginning of the list
*
* @param hList   The list object
*
* @return 
*
*/
void ArrayList_UnlinkFirst(ArrayList hList);

/**
* ArrayList_UnlinkLast
*
* Removes an element from the end of the list
*
* @param hList   The list object
*
* @return 
*
*/
void ArrayList_UnlinkLast(ArrayList hList);


/**
* ArrayList_GetFirst
*
* Gets the first list element
*
* @param hList   The list object
*
* @return the first element
*
*/
void *ArrayList_GetFirst(ArrayList hList);


/**
* ArrayList_GetLast
*
* Gets the last list element
*
* @param hList   The list object
*
* @return the last element
*
*/
void *ArrayList_GetLast(ArrayList hList);


/**
* ArrayList_GetItem
*
* Gets the index-th list element
*
* @param hList    The list object 
* @param uiIndex  The index-th list element 
*
* @return the index-th list element
*
*/
void *ArrayList_GetItem(ArrayList hList, uint32_t uiIndex);


/**
* ArrayList_IsEmpty
*
* True if list is empty
*
* @param hList   The list object
*
* @return true if list is empty, false otherwise
*
*/
bool ArrayList_IsEmpty(ArrayList hList);


/**
* ArrayList_SetAllocationStep
*
* Set the step used when growing the underlying array.
* When the first element is added, an array of uiStep 
* elements is created to store the list. When the list 
* needs to grow even more, room for uiStep more objects is allocated.
*
* @param hList   The list object
* @param uiStep  The allocation step (must be > 0)
*
*
*/
void ArrayList_SetAllocationStep(ArrayList hList, uint32_t uiStep);

/**
* ArrayList_FreeAll
*
* Call free() on and remove each one of the pointers in the list.
*
* @param list   The list object
*
*/
void ArrayList_FreeAll(ArrayList list);

#ifdef __cplusplus
}
#endif 

#endif // __ARRAY_LIST_H










