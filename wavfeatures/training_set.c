#include "training_set.h"
#include "array_list.h"
#include "error_utils.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

struct TrainingSet_s {
    char     *filename;
    ArrayList positive;
    ArrayList negative;
    ArrayList tagged_negatives;
};


struct WeakClassifier_s {
    int dim;
    char op;
    float threshold;
    float alpha;
};

#define OP_GT  1
#define OP_LT -1

struct WeakClassifier_s Classifiers[] = {
    {0, OP_GT, -55.4006, 2.63023357121},
    {25, OP_GT, 1228.8, 1.70040708324},
    {2, OP_LT, -42.6506, 0.956946832119},
    {23, OP_LT, 10.6531, 0.806064867838},
    {5, OP_LT, 0.132, 0.720334258397},
    {10, OP_LT, 4.756, 0.630374153257},
    {6, OP_LT, -47.1054, 0.631346266407},
    {23, OP_LT, 7.9144, 0.432267527492},
    {18, OP_GT, -9.9281, 0.388868877217},
    {5, OP_LT, 0.11, 0.384245189479},
    {24, OP_LT, 63.2295, 0.417600779035},
    {4, OP_LT, 7.4605, 0.426466124075},
    {7, OP_GT, -22.5171, 0.383548499067},
    {6, OP_LT, -53.2332, 0.473150554153},
    {20, OP_GT, 7.7604, 0.437274704825},
    {16, OP_LT, 0.4055, 0.450349429099},
    {24, OP_LT, 48.0545, 0.352916498745},
    {5, OP_LT, 0.154, 0.370846568724},
    {7, OP_GT, -15.1568, 0.329397054785},
    {15, OP_LT, 4.9666, 0.322599176079},
    {13, OP_GT, -1.4114, 0.400085445858},
    {20, OP_GT, 6.4916, 0.347515827401},
    {4, OP_LT, 7.4605, 0.349568477125},
    {2, OP_LT, -36.4257, 0.32014517582},
    {6, OP_LT, -53.2332, 0.345936115993},
    {23, OP_LT, 10.6531, 0.376842469791},
    {18, OP_GT, -3.5298, 0.344787337097},
    {19, OP_LT, 5.783, 0.34521296946},
    {24, OP_LT, 48.0545, 0.28095713124},
    {20, OP_LT, 12.8356, 0.355469083888},
    {4, OP_LT, 7.4605, 0.364546234353},
    {20, OP_GT, 6.4916, 0.259649330068},
    {18, OP_GT, -3.5298, 0.34187680449},
};


static float feature_vector_score(FeatureVector fv)
{
    int i;
    const int num_weak_classifiers = sizeof(Classifiers) / sizeof(Classifiers[0]);
    float score = 0.0;

    for (i=0; i < num_weak_classifiers; i++) {
        float value = fv->values[Classifiers[i].dim];
        int weak = (value < Classifiers[i].threshold) ? Classifiers[i].op : -Classifiers[i].op;

        score += (weak * Classifiers[i].alpha);
    }

    return score;
}

TrainingSet TrainingSet_Create(char * origin_filename)
{
    TrainingSet tset = malloc(sizeof(*tset));

    ArrayList_Create(&tset->positive);
    ArrayList_Create(&tset->negative);
    ArrayList_Create(&tset->tagged_negatives);

    tset->filename = origin_filename;

    return tset;
}


void TrainingSet_Destroy(TrainingSet tset)
{
    if (tset->positive) {
        ArrayList_FreeAll(tset->positive);
        ArrayList_Destroy(tset->positive);
        tset->positive = NULL;
    }

    if (tset->negative) {
        ArrayList_FreeAll(tset->negative);
        ArrayList_Destroy(tset->negative);
        tset->negative = NULL;
    }

    if (tset->tagged_negatives) {
        ArrayList_FreeAll(tset->tagged_negatives);
        ArrayList_Destroy(tset->tagged_negatives);
        tset->tagged_negatives = NULL;
    }

    free(tset);
}


/**
 * Add a feature vector to the training set.
 */
void TrainingSet_Add(TrainingSet tset, FeatureVector fvector)
{
    FeatureVector fv_copy;
    int data_size = sizeof(fvector->values[0]) * fvector->count;
    ArrayList     target_list;

    assert(tset);
    assert(fvector);

    // Make a deep copy of the feature vector
    fv_copy = malloc(sizeof(*fvector) + data_size);
    *fv_copy = *fvector;
    fv_copy->values = fv_copy->_float_align;
    memcpy(fv_copy->values, fvector->values, data_size);

    if (fv_copy->class_label == CLASS_POSITIVE) {
        target_list = tset->positive;
    }
    else if (fv_copy->class_label == CLASS_TAGGED) {
        target_list = tset->tagged_negatives;
    }
    else { // CLASS_NEGATIVE
        target_list = tset->negative;
    }

    ArrayList_LinkLast(target_list, fv_copy);
}


char *TrainingSet_GetOrigin(TrainingSet tset)
{
    return tset->filename;
}


typedef struct ScoredItem_s {
    float score;
    void *item;
} ScoredItem_t;

int score_cmp(const void * a, const void *b)
{
    ScoredItem_t *item_a = (ScoredItem_t *) a;
    ScoredItem_t *item_b = (ScoredItem_t *) b;

    if (item_a->score > item_b->score) {
        return 1;
    }
    else if  (item_a->score < item_b->score) {
        return -1;
    }
    else {
        return 0;
    }
}


int TrainingSet_PositiveCount(TrainingSet tset)
{
    return ArrayList_GetSize(tset->positive);
}


int TrainingSet_NegativeCount(TrainingSet tset)
{
    return ArrayList_GetSize(tset->negative);
}


int TrainingSet_TaggedCount(TrainingSet tset)
{
    return ArrayList_GetSize(tset->tagged_negatives);
}

void TrainingSet_ForEachPositive(TrainingSet tset, feature_vect_cb callback, void *opaque)
{
    uint32_t i;

    for (i = 0; i < ArrayList_GetSize(tset->positive); i++) {
        callback((FeatureVector) ArrayList_GetItem(tset->positive, i), opaque);
    }
}

void TrainingSet_ForEachNegative(TrainingSet tset, int negatives_needed, feature_vect_cb callback, void *opaque)
{
    int i, offset = 0;
    int candidates = ArrayList_GetSize(tset->negative);
    int needed = negatives_needed - ArrayList_GetSize(tset->tagged_negatives);
    ScoredItem_t *fv_scores = NULL;

    assert(callback);

    // First return all the tagged negatives
    for (i = 0; i < (int) ArrayList_GetSize(tset->tagged_negatives) ; i++) {
        callback((FeatureVector) ArrayList_GetItem(tset->tagged_negatives, i), opaque);
    }

    // We can't add more than we have...
    needed = MIN(needed, candidates);

    fv_scores = malloc(sizeof(fv_scores[0]) * candidates);

    // Give a score to every candidate
    for (i = 0; i < candidates; i++) {
        FeatureVector fv = (FeatureVector) ArrayList_GetItem(tset->negative, i);

        fv_scores[i].item = fv;
        fv_scores[i].score = feature_vector_score(fv);
    }

    // Sort according to score
    qsort(fv_scores, candidates, sizeof(fv_scores[0]), score_cmp);

    // Add the top scores to the results...
    for (i = 0; i < needed/2 ; i++) {
        callback((FeatureVector) fv_scores[i].item, opaque);
    }

    // We need a few less now..
    offset      = needed/2;
    needed     -= needed/2;

    // The rest should come randomly... so asign random scores to the remaining vectors and re-shuffle...
    for (i = offset; i < candidates; i++) {
        fv_scores[i].score = (100.0 * rand()/(float)RAND_MAX);
    }

    // Sort the rest with the random scores
    qsort(fv_scores + offset, candidates - offset, sizeof(fv_scores[0]), score_cmp);

    // Add the top (random) scores to the results...
    for (i = 0; i < needed ; i++) {
        callback((FeatureVector) fv_scores[offset + i].item, opaque);
    }

    free(fv_scores);
}


