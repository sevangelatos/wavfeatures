/*
 *  error_utils.h
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef ERROR_UTILS_H
#define ERROR_UTILS_H

#include <assert.h>
#include <stdio.h>

#define DBG_MESSAGES 1

#define FAILIF(X) do {                                                  \
                    if (X) {                                            \
                        if (DBG_MESSAGES) {                             \
                            fprintf(stderr, "%s:%d in %s cause:[%s]\n", \
                                   __FILE__, __LINE__, __FUNCTION__, (#X)); \
                        }                                               \
                        goto fail;                                      \
                    }} while(0)

#define NOT_USED(p) ((void)(p))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))


#endif // ERROR_UTILS_H
