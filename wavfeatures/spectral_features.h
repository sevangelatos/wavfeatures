/*
 *  spectral_features.h
 *
 *  Features calculated on the Power Spectral Density(PSD).
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef SPECTRAL_FEATURES_H
#define SPECTRAL_FEATURES_H


int SpectralFeatures(float *result, int result_len, float *in_psd, int psd_len, int sampling_rate);

#endif // SPECTRAL_FEATURES_H
