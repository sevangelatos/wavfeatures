/*
 *  statistics.c
 *  
 *  Statistics related methods and objects.
 *  
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include "statistics.h"
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

float Statistics_Mean(float *vector, int len)
{
    double sum = 0;
    int i;
    
    assert(vector);
    
    for (i = 0; i < len; i++) {
        sum += vector[i];
    }
    
    return (float) (sum / (double) len);
}


float Statistics_MaxAbs(float *vector, int len)
{
    float max = 0;
    
    assert(vector);
    
    int i;
    
    for (i=0; i < len; i++) {
        float abs_val = fabs(vector[i]);
        if (abs_val > max) {
            max = abs_val;
        }
    }
    
    return max;
}

float Statistics_Variance(float *vector, int len, float mean)
{
    float sum = 0;
    int i;

    assert(vector);

    for (i = 0; i < len; i++) {
        float delta = vector[i] - mean;
        sum += (delta * delta);
    }

    return sum / len;
}


float Statistics_StdDev(float *vector, int len, float mean)
{
    double sum = 0;
    int i;
    
    assert(vector);
    
    for (i = 0; i < len; i++) {
        float delta = vector[i] - mean;
        sum += (delta * delta);
    }
    
    return (float) sqrt(sum / (double) len);
}


float Statistics_Median3(float a, float b, float c)
{
    if (a > b) {
        if (b > c) {
            return b;
        }
        else if (a > c){
            return c;
        }
        else {
            return a;
        }
    }
    else {
        if (a > c) {
            return a;
        }
        else if (b > c){
            return c;
        }
        else {
            return b;
        }
    }
}


typedef struct ValueTtlPair_s {
    float value;    // value...
    int   ttl;      // Time to live...
} ValueTtlPair;


struct MovingMedian_s {
    int count;               // How many items do we currently have?
    int window;              // How many items of history do we keep?
    int dims;                // Dimensionality of items
    ValueTtlPair data[];     // Actual data from our window
};


static void moving_median_insert(ValueTtlPair *data, int size, int capacity, float value, int ttl)
{
    int i = 0, j = 0;
    int pos = 0;

    // Remove expired values
    while (i < size) {
        // Reduce the TTL of any previous frames
        data[i].ttl -= 1;

        if (data[i].ttl <= 0) {
            i++;
            continue;
        }

        if (data[i].value <= value) {
            pos = j + 1;
        }

        data[j++] = data[i++];
    }

    assert(pos >= 0 && pos < capacity);
    assert(j >= 0 && j <= capacity);

    // Make space for the new item
    for (i = j; i > pos ; i--) {
        data[i] = data[i-1];
    }

    data[pos].ttl   = ttl;
    data[pos].value = value;
}


void MovingMedian_Get(MovingMedian median, float *result, int dims)
{
    int i;
    int med = median->count / 2;
    ValueTtlPair *data = median->data;
    int window = median->window;

    for (i = 0; i < dims; i++) {
        result[i] = data[med].value;
        data += window;
    }
}


void MovingMedian_Update(MovingMedian median, float *values, int dims)
{
    int i = 0;
    int count, window;
    ValueTtlPair *data;

    assert(median);
    assert(values);
    assert(dims == median->dims);

    count  = median->count;
    window = median->window;
    data   = median->data;

    for (i = 0; i < dims; i++) {
        moving_median_insert(data, count, window, values[i], window);
        data += window; // Move to the next dimension;
    }

    if (count < median->window) {
        median->count += 1;
    }
}


MovingMedian MovingMedian_Create(int window, int dimensions)
{
    MovingMedian median = NULL;
    int size = sizeof(struct MovingMedian_s) + sizeof(ValueTtlPair) * window * dimensions;

    assert(window > 0);
    assert(dimensions > 0);

    median = calloc(1, size);

    if (median) {
        median->window = window;
        median->dims   = dimensions;
    }

    return median;
}


void MovingMedian_Destroy(MovingMedian median)
{
    free(median);
}

