/*
 *  spectral_features.c
 *
 *  Features calculated on the Power Spectral Density(PSD).
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */
#define INTERNAL_BUFF_SIZE 4096 

#include "spectral_features.h"
#include "dsp_utils.h"
#include "statistics.h"
#include <assert.h>

static float internal_buff[INTERNAL_BUFF_SIZE];

int SpectralRatios(float * result, int result_len, float *psd, int psd_len, int sampling_rate)
{
    float min_energy = 1.0e-9;
    float ball_band_energy;
    float voice_harmonics1_band;
    float voice_harmonics2_band;
    float voice_harmonics3_band;
    
    assert(result_len >= 7);
    
    // Add a bit of energy to avoid infinities and div by zero...
    result[0] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 150.0, 300.0) + min_energy;
    result[1] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 50.0, 100.0) + min_energy;
    result[2] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 200.0, 400.0) + min_energy;
    result[3] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 400.0, 700.0) + min_energy;
    result[4] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 700.0, 1400.0) + min_energy;
    result[5] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 1000.0, 2000.0) + min_energy;
    result[6] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 2000.0, 4000.0) + min_energy;
    result[7] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 4000.0, 6000.0) + min_energy;
    result[8] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 6000.0, 10000.0) + min_energy;
    result[9] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 790.0, 980.0) + min_energy;
    result[10] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 980.0, 1280.0) + min_energy;
    result[11] = DspUtils_BandEnergyHz(psd, psd_len, sampling_rate, 470.0, 600.0) + min_energy;

    ball_band_energy = result[0];
    voice_harmonics1_band = result[5];
    voice_harmonics2_band = result[6];
    voice_harmonics3_band = result[7];

    // Convert to dB relative to the band 150-300Hz where most of the energy lies
    DspUtils_VectorLinearToDecibel(result, result, ball_band_energy, 12);

    result[0]  = DspUtils_Decibel(ball_band_energy);
    result[12] = DspUtils_Decibel(voice_harmonics2_band / voice_harmonics1_band);
    result[13] = DspUtils_Decibel(voice_harmonics2_band / voice_harmonics3_band);

    // The number of features we added
    return 14;
}

int Spectral_PeakinessFeatures(float *result, int result_len, float *in_psd, int psd_len, int sampling_rate)
{
    float *log_spectrum = internal_buff;
    const int log_spectum_len = 100;
    float mean;
    int num_features = 0;

    assert(result_len >= 2);
    assert(log_spectum_len * sizeof(float) <= INTERNAL_BUFF_SIZE);

    DspUtils_Psd2LogScale(log_spectrum, log_spectum_len, in_psd, psd_len, sampling_rate);
    DspUtils_VectorLinearToDecibel(log_spectrum, log_spectrum, 1.0, log_spectum_len);

    // Clip the psd at +/- 85dB to avoid having to deal with infinities
    DspUtils_VectorClip(log_spectrum, log_spectrum, log_spectum_len, -85, 85);


    mean = Statistics_Mean(log_spectrum, log_spectum_len);
    result[num_features++] = Statistics_StdDev(log_spectrum, log_spectum_len, mean);

    // Center to 0 dB
    DspUtils_VectorAddScalar(log_spectrum, log_spectrum, log_spectum_len, -mean ); // To avoid infinities

    // Zero crossings rate
    result[num_features++] = DspUtils_ZeroCrossingRate(log_spectrum, log_spectum_len);

    return num_features;
}

int SpectralFeatures(float *result, int result_len, float *in_psd, int psd_len, int sampling_rate)
{
    float mean, diff_mean;
    float *psd = internal_buff;
    int num_features = 0;
    
    assert(psd_len <= INTERNAL_BUFF_SIZE);
    assert(result_len >= 9);
    
    num_features += SpectralRatios(result, result_len, in_psd, psd_len, sampling_rate);
    assert(num_features < result_len);

    num_features += Spectral_PeakinessFeatures(result + num_features,
                                               result_len - num_features, in_psd, psd_len, sampling_rate);
    assert(num_features < result_len);

    // Convert PSD to decibel
    DspUtils_VectorLinearToDecibel(psd, in_psd, 1.0, psd_len);
    
    // Clip the psd at +/- 96dB to avoid having to deal with infinities
    DspUtils_VectorClip(psd, psd, psd_len, -96, 96);
    
    // Mean dB value
    mean = Statistics_Mean(psd, psd_len);
    
    assert(num_features < result_len);
    // StdDev of psd
    result[num_features++] = Statistics_StdDev(psd, psd_len, mean);
    
    // Calculate the derivative of psd
    assert(num_features < result_len);
    DspUtils_Derivative(psd, psd, psd_len);
    diff_mean = Statistics_Mean(psd, psd_len);
    result[num_features++] = Statistics_StdDev(psd, psd_len, diff_mean);
    // Zero crossings rate of derivative of psd
    result[num_features++] = DspUtils_ZeroCrossingRate(psd, psd_len);

    // Now calculate the
    
    return num_features;
}
