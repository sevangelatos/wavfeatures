/*
 *  attackdetector.c
 *
 *  Detect the attack of a new sound by analyzing the
 *  spikes in each octave of the spectrum.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#include "attackdetector.h"
#include "statistics.h"
#include "error_utils.h"
#include "dsp_utils.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>


struct AttackDetector_s {
    int    psd_len;        /* lenght of the psd */
    int    octaves;        /* Number of octaves covered */
    MovingMedian median;   /* Moving median estimator */
    float median_buffer[]; /* Temp buffer to retrieve current median and push the next one. */
};


AttackDetector AttackDetector_Create(int window_size, int psd_len)
{
    int i, octaves = 0;
    AttackDetector detect;

    // octaves = log2(psd_len/2)
    for (i = psd_len/2; i > 0; i /= 2) {
        octaves += 1;
    }

    detect = calloc(1, sizeof(*detect) + sizeof(float) * octaves);
    detect->median  = MovingMedian_Create(window_size, octaves);
    detect->psd_len = psd_len;
    detect->octaves = octaves;

    assert(detect->octaves > 0);

    for (i=0; i < window_size; i++) {
        MovingMedian_Update(detect->median, detect->median_buffer, octaves);
    }

    return detect;
}


void AttackDetector_Destroy(AttackDetector detect)
{
    if (detect && detect->median) {
        MovingMedian_Destroy(detect->median);
    }
    free(detect);
}


float AttackDetector_Update(AttackDetector detect, float *psd, int psd_len)
{
    int i, octave;
    int hi = psd_len;   // Index of octave upper limit
    int lo = hi / 2;    // Index of octave lower limit
    float max_energy = pow(10, -8.0);

    assert(detect);
    assert(detect->psd_len == psd_len);

    MovingMedian_Get(detect->median, detect->median_buffer, detect->octaves);

    for (octave = detect->octaves - 1; lo > 0; octave -= 1) {
        double octave_energy = 0;
        float  base_level, energy_diff;


        for (i = lo; i < hi; i++) {
            octave_energy += psd[i];
        }

        // Account for the negative frequencies
        octave_energy *= 2.0;

        base_level = detect->median_buffer[octave];
        energy_diff = MAX(octave_energy - base_level, 0);

        // Replace the value in the medians buffer
        detect->median_buffer[octave] = octave_energy;

        if (energy_diff > max_energy) {
            max_energy = energy_diff;
        }

        //printf ("o(%d)=%0.5lf", lo, DspUtils_Decibel(octave_energy));

        hi = lo;
        lo /= 2;
    }

    //float max_db = DspUtils_Decibel(max_energy);
    //printf ("attack: %lf %s\n", max_db, ((max_db >= -41.5)?"DONG!":""));

    // Add the current energies in the history buffer...
    MovingMedian_Update(detect->median, detect->median_buffer, detect->octaves);

    return max_energy;
}


