/*
 *  main.c
 * 
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#define __STDC_LIMIT_MACROS
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include <libgen.h>
#include "wav_reader.h"
#include "error_utils.h"
#include "statistics.h"
#include "dsp_utils.h"
#include "feature_extractor.h"

#define MAX_CUE_POINTS 256
#define FEATURE_HISTORY 40 // How many windows the feature extractor keeps within it
#define BUFFERED_CHUNKS 10
#define CUE_DURATION_MILLISEC 15 // Duration of a cue
#define COVERAGE 4
#define FFT_SIZE 512

/* Global variables */
static CuePoint cue_points[MAX_CUE_POINTS];
static int      num_cue_points = 0;


typedef struct PrintArgs_s {
    FILE *fp;
    char* origin_filename;
    int sampling_rate;
} PrintArgs_t;

static int cue_cmp(const void *a, const void *b)
{
    const CuePoint cpoint_a = *((CuePoint*)a);
    const CuePoint cpoint_b = *((CuePoint*)b);

    return WavReader_CuePointGetPosition(cpoint_a) - WavReader_CuePointGetPosition(cpoint_b);
}

static void gather_cue_points(WavReader hWav)
{
    CuePoint cpoint = NULL;

    while ((cpoint = WavReader_GetNextCuePoint(hWav, cpoint)) != NULL) {
        fprintf(stderr, "\tCue point %u at sample %u with length %u \n",
               WavReader_CuePointGetId(cpoint),
               WavReader_CuePointGetPosition(cpoint),
               WavReader_CuePointGetLength(cpoint));
        fprintf(stderr, "\t\tlabel: [%s]\n", WavReader_CuePointGetLabel(cpoint));
        fprintf(stderr, "\t\ttext:  [%s]\n", WavReader_CuePointGetText(cpoint));
        fprintf(stderr, "\t\tnote:  [%s]\n", WavReader_CuePointGetNote(cpoint));

        cue_points[num_cue_points] = cpoint;
        num_cue_points += 1;
    }

    // Sort the cue points according to their position in the wav stream.
    qsort(cue_points, num_cue_points, sizeof(CuePoint), cue_cmp);
}

static int get_class_label(int cue_duration, int32_t sample_offset, int len)
{
    int i;
    uint32_t overlap_threshold = cue_duration / COVERAGE;
    uint32_t sample_end = sample_offset + len;


    for (i = 0; i < num_cue_points; i++) {
        CuePoint cue = cue_points[i];
        uint32_t cue_pos = WavReader_CuePointGetPosition(cue);
        uint32_t cue_end = cue_pos + cue_duration;

        if (sample_offset + overlap_threshold >= cue_end) {
            // The sample starts after the cue point
            continue;
        }
        else if (cue_pos + overlap_threshold >= sample_end) {
            // The sample ends before the cue point. No point in searching
            // other cue points as they are sorted
            break;
        }

        else {
            // Well, if we reach this point, they do overlap...
            if (WavReader_CuePointGetLabel(cue)[0] == 'R') {
                return CLASS_POSITIVE;
            }
            else {
                return CLASS_TAGGED;
            }
        }
    }

    return CLASS_NEGATIVE;
}

/**
 * Read signal_len bytes of audio data from the first channel.
 * Returns the number of samples actually read
 */
int wav_read_audiodata (int *sample_offset, WavReader hWav, int16_t* io_buff, int io_len, float *audio, int audio_len)
{
    int samples_read = 0;
    int offset = *sample_offset;
    
    assert(hWav->block_align * audio_len <= io_len);
    assert(hWav->bits_per_sample == 16);
        
    int read = WavReader_ReadAudio(hWav, offset, io_buff, hWav->block_align * audio_len);
    
    if (read < 0) {
        return read;
    }

    samples_read = read / hWav->block_align;
    *sample_offset = offset + samples_read;
    
    DspUtils_Int16ToFloat(audio, io_buff, hWav->channels, read / sizeof(io_buff[0]));
    
    return samples_read;
}


/**
 * Print to a file stream a feature vector.
 */
static void print_feature_vector(FeatureVector feature_vector, void *opaque)
{
    PrintArgs_t *args = opaque;
    FILE *fp = args->fp;
    char* filename = basename(args->origin_filename);

    // Print out the filename and timestamp
    fprintf(fp, "%s %06lld ", filename, (1000 * feature_vector->sample_offset) / args->sampling_rate);

    // Print the feature vector
    DspUtils_VectorPrint(fp, feature_vector->values, feature_vector->count, false);

    // And finally the label
    fprintf(fp, " %s\n", (feature_vector->class_label == CLASS_POSITIVE) ? "P" : "N");
}


/**
 * Extract a feature vector, timing and class ownership info for an audio chunk.
 */
void chunk_analyze(int sample_offset, float *audio, int audio_len, int cue_duration, FeatureExtractor fe, PrintArgs_t *print_args)
{
    FeatureVector feature_vector;

    // Extract the feature vector
    feature_vector = FeatureExtractor_Extract(fe, audio, audio_len, sample_offset);

    if (feature_vector) {
        feature_vector->class_label = get_class_label(cue_duration, feature_vector->sample_offset, audio_len);
        print_feature_vector(feature_vector, print_args);

    }
}


/**
 * Read audio in (possibly overlapping) chunks and call chunk_analyze() for each chunk.
 */
void wav_split_chunks(WavReader hWav, PrintArgs_t *print_args, int chunk_len, int step)
{
    int buffer_size = BUFFERED_CHUNKS * chunk_len * hWav->block_align;
    int sampling_rate = hWav->sampling_rate;
    int16_t *buff;
    int smpl_offset = 0;
    int read = 0;
    int remain = 0; // Number of samples retained in the audio buffer from the previous read...
    int audio_pos = 0;
    int audio_len = BUFFERED_CHUNKS * chunk_len;
    int cue_duration = (sampling_rate * CUE_DURATION_MILLISEC) / 1000;
    float *audio;
    FeatureExtractor fe = FeatureExtractor_Create(sampling_rate, chunk_len, FEATURE_HISTORY);
    FeatureVector fv;

    // Allocate audio vector and buffer
    audio = DspUtils_CreateVector(audio_len);
    buff  = malloc(buffer_size);

    while ((read = wav_read_audiodata(&smpl_offset, hWav, buff, buffer_size, audio + remain, audio_len - remain)) > 0) {
        int offset;

        read += remain;

        for (offset = 0; offset + chunk_len <= read; offset += step) {
            // Analyze all chunks in the audio buffer
            chunk_analyze(audio_pos + offset, audio+offset, chunk_len, cue_duration, fe, print_args);
        }

        remain = read - offset;

        // Move to the beginning of the buffer the remaining data.
        if (remain > 0) {
            memmove(audio, audio+offset, remain * sizeof(audio[0]));
        }

        audio_pos = smpl_offset - remain;
    }

    // Force some data into the extractor to force out the accumulated vectors
    while ((fv = FeatureExtractor_GetFinal(fe)) != NULL) {
        fv->class_label = get_class_label(cue_duration, fv->sample_offset, chunk_len);
        print_feature_vector(fv, print_args);
    }

    FeatureExtractor_Destroy(fe);
    free(audio);
    free(buff);
}


int main(int argc, char *argv[])
{
    int i = 1;
    PrintArgs_t print_args;

    NOT_USED(argc);

    while (argv[i]) {
        char *filename = argv[i++];
        errno = 0;
        WavReader hWav = NULL;

        hWav = WavReader_OpenFile(filename);
        
        if (NULL == hWav) {
            if (errno) { perror(filename); }
            fprintf(stderr, "Skipping %s\n", filename);
            continue;
        }
        
        fprintf(stderr, "%s: %u ch %u Hz %u bit\n", filename, hWav->channels,
               hWav->sampling_rate, hWav->bits_per_sample);

        gather_cue_points(hWav);

        print_args.origin_filename = filename;
        print_args.sampling_rate = hWav->sampling_rate;
        print_args.fp = stdout;

        // Split the wav in chunks and populate the training set
        wav_split_chunks(hWav, &print_args, FFT_SIZE, FFT_SIZE/2);
        
        WavReader_Close(hWav);
        num_cue_points = 0;
    }

    return 0;
}

