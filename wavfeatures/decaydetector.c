/*
 *  decaydetector.c
 *
 *  Detect the decay of a signal by inspecting the
 *  history of the dB power of the signal.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */
#include "decaydetector.h"
#include "statistics.h"
#include "error_utils.h"
#include "dsp_utils.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <float.h>
#include <math.h>

struct DecayDetector_s {
    int   counter;     /* Mod window_len counter  */
    int   window_len;  /* How many values back do we remember ? */
    float threshold;   /* How many dB of decay do we expect within the window? */
    float history[];   /* history of the last window_len RMS dB values */
};


DecayDetector DecayDetector_Create(int length, float threshold_db)
{
    int history_bytes = sizeof(float) * length;
    int i;
    DecayDetector detect = calloc(1, sizeof(*detect) + history_bytes);

    assert(length > 0);
    assert(threshold_db > 0);

    detect->counter    = 0;
    detect->window_len = length;
    detect->threshold  = threshold_db;

    for (i=0; i<length; i++) {
        detect->history[i] = -INFINITY;
    }

    return detect;
}


void DecayDetector_Destroy(DecayDetector detect)
{
    free(detect);
}


float DecayDetector_Update(DecayDetector detect, float rms_db)
{
    int i;
    int i_first;
    float *history, delta;

    assert(detect);

    history = detect->history;

    history[detect->counter] = rms_db;

    //increment the counter
    detect->counter = (detect->counter + 1) % detect->window_len;

    // Now, this is the first item that got in the buffer
    i_first = detect->counter;

    float first = history[i_first];

    delta = first - rms_db;
    if (delta > detect->threshold) {
        int  prev = i_first;

        // Maximum allowed rippling during the decay...
        float max_rippling = delta / 2;

        for (i = 1; i < detect->window_len; i++) {
            // Todo: maybe optimize with a mask when window_len is power of 2?
            int current = (i_first + i) % detect->window_len;

            // If we see too much rippling, then it's not really decay..
            if ( history[current] - history[prev]  > max_rippling) {
                return 0;
            }

            prev = current;
        }

        return delta;
    }

    return 0;
}
