/*
 *  dsp_utils.c
 *
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#define __STDC_LIMIT_MACROS
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "dsp_utils.h"
#include "error_utils.h"

void Dft_Fin();

/* --------------- Local function declarations ----------------- */

/* --------------- Public function definitions ----------------- */

void DspUtils_Init()
{
    // Do nothing :-)
}


void DspUtils_Fin()
{
    Dft_Fin();
}


float *DspUtils_WindowHann(int len)
{
    int i;
    float *window = malloc(len * sizeof(window[0]));

    assert (len > 1);

    if (NULL == window) {
        return NULL;
    }

    for (i=0; i < len; i++) {
        window[i] = 0.5 - 0.5 * cos((2.0 * PI * i) / (len - 1));
    }

    return window;
}


float *DspUtils_WindowHamming(int len)
{
    int i;
    float *window = malloc(len * sizeof(window[0]));

    assert (len > 1);

    if (NULL == window) {
        return NULL;
    }

    for (i=0; i < len; i++) {
        window[i] = 0.54 - 0.46 * cos((2.0 * PI * i) / (len - 1));
    }

    return window;

}


float *DspUtils_CreateVector(int len)
{
    return (float*) calloc(len, sizeof(float));
}

void DspUtils_Derivative(float *derivative, float *input, int len)
{
    int i;
    
    for (i = 0; i < len - 1; i++) {
        derivative[i] = input[i+1] - input[i];
    }
    
    if (len > 1) {
        derivative[len-1] = 0.0;
        derivative[len-1] = derivative[MAX(len - 2, 0)];
    }
}


void DspUtils_VectorEntrywiseProduct(float *result, float *a, float *b, int len)
{
    int i;

    assert(result);
    assert(a);
    assert(b);

    for (i = 0; i < len; i++) {
        result[i] = a[i] * b[i];
    }
}

void DspUtils_VectorLinearToDecibel(float *decibel, float *input, float level_db0, int len)
{
    int i;
    double db0;
    
    assert(decibel);
    assert(input);
    
    db0 = 10.0 * log10(level_db0);
    
    for (i = 0; i < len; i++) {
        decibel[i] = 10.0f * log10(input[i]) - db0;
    }
}

float DspUtils_VectorSum(float *input, int len)
{
    int i;
    float sum = 0;
    for (i=0; i < len; i++) {
        sum += input[i];
    }
    return sum;
}


void DspUtils_VectorClip(float *output, float *input, int len, float min, float max)
{
    int i;
    
    assert(output);
    assert(input);
    assert(len >= 0);
    
    for (i = 0; i < len; i++) {
        if (input[i] > max) {
            output[i] = max;
        }
        else if (input[i] < min) {
            output[i] = min;
        }
        else {
            output[i] = input[i];
        }
    }
}

void DspUtils_VectorAddScalar(float *result, float *input, int len, float add)
{
    int i;

    assert(result);
    assert(input);

    for (i = 0; i < len; i++) {
        result[i] += add;
    }
}


void DspUtils_VectorScale(float *result, float *a, float factor, int len)
{
    int i;

    assert(result);
    assert(a);

    for (i = 0; i < len; i++) {
        result[i] = a[i] * factor;
    }
}

float DspUtils_EnergyFromPSD(float *psd, int psd_len)
{
    double energy = 0;

    energy += psd[0];
    //x2 to account for the power in the negative frequency also
    energy += 2.0f * DspUtils_VectorSum(psd + 1, psd_len - 1);

    return energy;
}

float DspUtils_FftRealHz2DftScale(int fft_len, int sampling_rate, float freq)
{
    float bin;
    
    assert(2.0*freq <= sampling_rate && freq >= 0);
    assert(fft_len >= 2 && POW_OF_2(fft_len));
    assert(sampling_rate > 0);
    
    bin = (freq * fft_len) / sampling_rate;
        
    return bin;
}

/* Resample the psd to a logarithmic frequency scale */
void DspUtils_Psd2LogScale(float *log_spectrum, int spectrum_len, float *psd, int psd_len, int sample_rate)
{
    float low, high = sample_rate/2.0;
    float log_psd_len = DspUtils_Log2(psd_len - 1);
    int i;
    
    high = 1.0;
    for (i = 0; i < spectrum_len; i++) {
        low =  high;
        high = powf(2.0, log_psd_len * ((i + 1)/(float)spectrum_len));
        log_spectrum[i] = DspUtils_BandEnergyBins(psd, psd_len, low, high);
    }
    
}


float DspUtils_BandEnergyBins(float *psd, int psd_len, float low, float high)
{
    float e_low, e_high, energy;
    int floor_low, ceil_high;
    
    assert (low <= high);
            
    floor_low = floorf(low);
    ceil_high = ceilf(high);
    
    //Energies out of the lower and higher boundary, to be subtracted
    e_low  = (floor_low + 1 < psd_len) ? psd[floor_low + 1] * (low - floor_low) : 0.0;
    e_high = (ceil_high < psd_len) ? psd[ceil_high] * (ceil_high - high) : 0.0;
    
    // Total energy covering the wanted area
    energy = DspUtils_VectorSum(psd + floor_low + 1, ceil_high - floor_low);
    
    // Subtract the unneeded parts for more accurate result
    energy -= (e_low + e_high);
    
    // Account for negative frequencies
    return 2.0 * energy;
}


float DspUtils_BandEnergyHz(float *psd, int psd_len, int sample_rate, float low, float high)
{
    int fft_len = 2 * (psd_len - 1);
    
    assert (low <= high);
    
    // Convert to the PSD scale
    low  = DspUtils_FftRealHz2DftScale(fft_len, sample_rate, low);
    high = DspUtils_FftRealHz2DftScale(fft_len, sample_rate, high);
    
    return DspUtils_BandEnergyBins(psd, psd_len, low, high);
}


float DspUtils_ZeroCrossingRate(float *input, int len)
{
    bool sign;
    int  i, crossings = 0;

    assert(len > 0);

    sign = input[0] >= -0.0;

    for (i=0; i < len; i++) {
        bool new_sign = input[i] >= 0.0;

        if (new_sign != sign) {
            crossings += 1;
            sign = new_sign;
        }
    }

    return (100 * crossings) / (float)len;
}


float DspUtils_Power(float *input, int len)
{
    int i = 0;
    double energy = 0;

    for (i = 0; i < len; i++) {
        energy += SQUARE(input[i]);
    }

    energy /= (double) len;
    return energy;
}


float DspUtils_RMS(float *input, int len)
{
    return sqrt(DspUtils_Power(input, len));
}

float DspUtils_FftRealBin2Hz(int fft_len, int sampling_rate, int bin)
{
    assert(bin >= 0 && bin <= fft_len/2);
    assert(fft_len >= 2 && POW_OF_2(fft_len));

    return (bin * (double) sampling_rate) / fft_len;
}

int DspUtils_FftRealHz2Bin(int fft_len, int sampling_rate, float freq)
{
    int bin;
    
    assert(2.0*freq <= sampling_rate && freq >= 0);
    assert(fft_len >= 2 && POW_OF_2(fft_len));
    assert(sampling_rate > 0);
    
    bin = rintf((freq * fft_len) / sampling_rate);
    
    if (2 * bin > fft_len) {
        bin = fft_len / 2;
    }
    
    return bin;
}



/*
 * Convert the output of a fourier to
 * a Spectral Power Distribution.
 * fourier format should be:
 * [DC, Nyquist, Rel(1), img(1), Rel(2), Img(2), ...]
*/
void DspUtils_FourierToPsd(float *psd, int psd_len, float *fourier, int fourier_len)
{
    int i, n;

    assert(psd);
    assert(fourier);
    assert(fourier_len >= 2);
    assert(psd_len >= 2);
    assert(psd_len == 1 + fourier_len/2);

    // DC component
    psd[0] = SQUARE(fourier[0]);

    // Nyquist component
    psd[psd_len - 1] = SQUARE(fourier[1]);
    
    for (i = 2, n = 1; i < fourier_len - 1; i += 2) {
        psd[n++] = SQUARE(fourier[i]) + SQUARE(fourier[i + 1]);
    }

    assert(n == psd_len - 1);
}


void DspUtils_Int16ToFloat(float *result, int16_t *input, int stride, int len)
{
    const double max = (double) (INT16_MAX);
    int i;

    for (i = 0; i <= len - stride; i += stride) {
        result[i] = input[i] / max;
    }

}

void DspUtils_VectorPrint(FILE * fp, float * vect, uint32_t len, bool newline)
{
    uint32_t i;
    
    assert(fp);
    assert(vect);
    
    for (i=0; i < len; i += 1) {
        fprintf(fp, "%+09.03lf%s", vect[i], ((i + 1 < len) ? " " : ""));
    }
        
    if (newline) {
        fputc('\n', fp);
    }
    
}


int DspUtils_Log2(uint32_t n)
{
    uint32_t i = 0;
    
    assert (POW_OF_2(n));
    
    for (i=0; i < 31; i++) {
        if (n == ( ((uint32_t) 0x1) << i)) {
            return (int) i;
        }
    }
    
    return -1;
}

void DspUtils_PrintPsd(FILE *file, float *psd, int psd_len, int sample_rate)
{
    int i, fft_len = 2 * (psd_len - 1);

    assert(POW_OF_2(fft_len));
    assert(psd);
    assert(file);

    for (i=0; i < psd_len; i++) {
        float freq = DspUtils_FftRealBin2Hz(fft_len, sample_rate, i);
        float power = psd[i];
        fprintf(file, "%.3f %.3f\n", freq, power);
    }
}

double DspUtils_Decibel(double a)
{
    return 10.0 * log10(a);
}

double DspUtils_Decibel2Linear(double db)
{
    return pow(10.0, db / 10.0);
}

