/*
 *  file_io.h
 *
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef FILE_IO_H
#define FILE_IO_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct IO_s *IO;

struct IO_s {

    /* Read raw data */
    int (*read)(IO io, void *buffer, int len);

    /* Read little endian double word */
    uint32_t (*le_dword)(IO io);

    /* Read little endian word */
    uint16_t (*le_word)(IO io);

    /* Read little endian byte */
    uint8_t (*byte)(IO io);

    /* Have we reached EOF? */
    bool (*eof)(IO io);

    /* close the IO object */
    void (*close)(IO io);

    /* string, make sure there is len+1 bytes space in str for the '\0'*/
    char * (*bstring)(IO io, char *str, int len);
    
    /* seek */
    bool (*seek)(IO io, off_t offset, int whence);

    /* current position */
    int64_t (*tell)(IO io);
    
    /* -- Private stuff -- */
    FILE *_file;
};

typedef struct IO_s IO_t;

bool IO_from_filename(IO io, const char *filename);

#endif // FILE_IO_H
