/*
 *  dsp_apple.c
 *  
 *  Apple specific implementations of DSP functions.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */


/*
 * See documentation here:
 * http://developer.apple.com/library/ios/#documentation/Performance/Conceptual/vDSP_Programming_Guide/SampleCode/SampleCode.html
 */

#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <Accelerate/Accelerate.h>

#include "dsp_utils.h"
#include "error_utils.h"


struct FFTReal_s {
    COMPLEX_SPLIT   complex_buff;
    FFTSetup        setup;
    int             len;
    int             log2_len;
};


FFTReal DspUtils_CreateFFTReal (int len)
{
    FFTReal fft = NULL;
    int len_over_2 = len / 2;
    
    assert (POW_OF_2(len));
    
    FAILIF((fft = calloc(1, sizeof(*fft))) == NULL);
    
    fft->len = len;
    fft->log2_len = DspUtils_Log2(len);
    FAILIF(fft->log2_len <= 0);
    
    // Allocate space for the fft to happen in-place.
    fft->complex_buff.realp = (float *) malloc(len_over_2 * sizeof(float));
    fft->complex_buff.imagp = (float *) malloc(len_over_2 * sizeof(float));
    FAILIF (fft->complex_buff.realp == NULL || fft->complex_buff.imagp == NULL);
    
    fft->setup = vDSP_create_fftsetup(fft->log2_len, FFT_RADIX2);
    FAILIF (fft->setup == NULL);

    return fft;
    
fail:
    DspUtils_DestroyFFTReal (fft);
    return NULL;
}


void DspUtils_DestroyFFTReal (FFTReal fft)
{
    if (fft == NULL) {
        return;
    }
    
    free(fft->complex_buff.realp);
    fft->complex_buff.realp = NULL;
    
    free(fft->complex_buff.imagp);
    fft->complex_buff.imagp = NULL;
    
    if (fft->setup) {
        vDSP_destroy_fftsetup(fft->setup);        
    }

    free (fft);
}


/*
 * Output array packed like in:
 *
 * http://developer.apple.com/library/ios/documentation/Performance/Conceptual/vDSP_Programming_Guide/UsingFourierTransforms/UsingFourierTransforms.html#//apple_ref/doc/uid/TP40005147-CH202-15398
 */
void DspUtils_FftReal(FFTReal fft, float *input, float *output, int len)
{
    int len_over_2;
    
    assert(fft);
    assert(fft->len == len);
    assert(len >= 2);
    assert(input);
    assert(output);
    
    len_over_2 = len / 2;
    
    /* Copy input data to the buffer...
     * Look at the real signal as an interleaved complex vector by
     * casting it.  Then call the transformation function vDSP_ctoz to
     * get a split complex vector, which for a real signal, divides into
     * an even-odd configuration. */
    vDSP_ctoz((COMPLEX *) input, 2, &fft->complex_buff, 1, len_over_2);
    
    // Execute the FFT
    vDSP_fft_zrip(fft->setup, &fft->complex_buff, 1, fft->log2_len, FFT_FORWARD);
    
    // Copy out the output
    vDSP_ztoc(&fft->complex_buff, 1, (COMPLEX *) output, 2, len_over_2);
}

double DspUtils_GetFftRealAmplification(FFTReal fft)
{
    return 2.0 * fft->len;
}

void Dft_Fin()
{
    
}
