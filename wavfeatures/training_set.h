#ifndef TRAINING_SET_H
#define TRAINING_SET_H

#include "feature_extractor.h"


typedef struct TrainingSet_s *TrainingSet;

typedef void feature_vect_cb(FeatureVector feature_vector, void *opaque);

TrainingSet TrainingSet_Create(char *origin_filename);


void TrainingSet_Destroy(TrainingSet tset);

/**
 * Add a feature vector to the training set.
 */
void TrainingSet_Add(TrainingSet tset, FeatureVector fvector);

char * TrainingSet_GetOrigin(TrainingSet tset);

void TrainingSet_ForEachNegative(TrainingSet tset, int negatives_needed, feature_vect_cb callback, void *opaque);

void TrainingSet_ForEachPositive(TrainingSet tset, feature_vect_cb callback, void *opaque);

int TrainingSet_PositiveCount(TrainingSet tset);

int TrainingSet_NegativeCount(TrainingSet tset);

int TrainingSet_TaggedCount(TrainingSet tset);

#endif // TRAINING_SET_H
