// Includes -----------------------------------------------------------------
#include "array_list.h"
#include "error_utils.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

// Typedefines and defines -----------------------------------------------------

#define DEFAULT_ALLOCATION_STEP  50

typedef struct ArrayList_s {
  uint32_t      uiSize;           // The number of list elements
  uint32_t      uiFirst;          // The index of the first element
  uint32_t      uiCapacity;       // The size of apArray
  uint32_t      uiAllocationStep; // How many more elements we allocate when we realloc the array
  void       **apArray;         // The array where elements are stored
} ArrayList_t;


// Global variables ------------------------------------------------------------
#define INCREMENT(X,Y) ( ((X) + 1 < (Y)) ? ((X) + 1):0 )
#define DECREMENT(X,Y) ( ((X) == 0) ? ((Y) - 1):((X) - 1) )

static bool array_list_grow(ArrayList hList);

// Local variable --------------------------------------------------------------


// Local procedures ------------------------------------------------------------


// Procedures ------------------------------------------------------------------


/**
* ArrayList_Create
*
* Creates a list object
*
* @param phList   On return it will contain the list handle
*
* @return true if successful, false otherwise.
*
*/
bool ArrayList_Create(ArrayList *phList) 
{
  ArrayList hList = NULL;

  assert(phList);

  hList = calloc(1, sizeof(*hList));
  FAILIF(NULL == hList);
  
  hList->uiFirst = 0;
  hList->uiSize  = 0;
  hList->uiCapacity = 0;
  hList->uiAllocationStep = DEFAULT_ALLOCATION_STEP;
  hList->apArray = NULL;
  
  *phList = hList;
  return true;
  
fail:
  ArrayList_Destroy(hList);
  return false;
}

/**
* ArrayList_Destroy
*
* Destroys a list object
*
* @param hList    The list object to destroy
*
* @return 
*
*/
void ArrayList_Destroy(ArrayList hList) 
{
  if(hList) {
    // Maybe you are leaking memory?
    assert(0 == hList->uiSize);
    free(hList->apArray);
    free(hList);
  }
}

/**
* ArrayList_GetSize
*
* Gets the number of list elements
*
* @param hList  The list object
*
* @return the number of list elements
*
*/
uint32_t ArrayList_GetSize(ArrayList hList)
{
  assert(hList);
  return hList->uiSize;
}

/**
* ArrayList_LinkFirst
*
* Adds an element at the beginning of the list
*
* @param hList   The list object
* @param pvData  The data to add
*
* @return true on success, false otherwise
*
*/
bool ArrayList_LinkFirst(ArrayList hList, void *pvData) 
{
  assert(hList);
  
  if (hList->uiSize == hList->uiCapacity) {
    FAILIF(array_list_grow(hList) == false);
  }
  
  assert(hList->uiFirst < hList->uiCapacity);
  assert(hList->uiSize < hList->uiCapacity);
  hList->uiSize++;
  hList->uiFirst = DECREMENT(hList->uiFirst, hList->uiCapacity);
  hList->apArray[hList->uiFirst] = pvData;
  return true;
  
fail:
  return false;
}

/**
* ArrayList_LinkLast
*
* Adds an element at the end of the list
*
* @param hList   The list object
* @param pvData  The data to add
*
* @return true on success, false otherwise
*
*/
bool ArrayList_LinkLast(ArrayList hList, void *pvData) 
{
  uint32_t uiLast;
  
  assert(hList);
  
  if (hList->uiSize == hList->uiCapacity) {
    FAILIF(array_list_grow(hList) == false);
  }
  
  assert(hList->uiFirst < hList->uiCapacity);
  
  uiLast = hList->uiFirst + hList->uiSize;
  // Wrap around
  if(uiLast >= hList->uiCapacity) {
    uiLast -= hList->uiCapacity;
  }
  
  assert(uiLast < hList->uiCapacity);
  
  hList->uiSize++;
  hList->apArray[uiLast] = pvData;
  return true;
  
fail:
  return false;
}

/**
* ArrayList_UnlinkFirst
*
* Removes an element from the beginning of the list
*
* @param hList   The list object
*
* @return 
*
*/
void ArrayList_UnlinkFirst(ArrayList hList) 
{
  assert(hList);
  assert(hList->uiSize > 0);
  assert(hList->uiFirst < hList->uiCapacity);
  
  hList->uiSize--;
  hList->uiFirst = INCREMENT(hList->uiFirst, hList->uiCapacity);
}


/**
* ArrayList_UnlinkLast
*
* Removes an element from the end of the list
*
* @param hList   The list object
*
* @return 
*
*/
void ArrayList_UnlinkLast(ArrayList hList) 
{
  assert(hList);
  assert(hList->uiSize > 0);
  assert(hList->uiFirst < hList->uiCapacity);
  
  hList->uiSize--;
}


/**
* ArrayList_GetFirst
*
* Gets the first list element
*
* @param hList   The list object
*
* @return the first element
*
*/
void *ArrayList_GetFirst(ArrayList hList) 
{
  assert(hList);
  assert(hList->uiSize > 0);
  assert(hList->apArray);
  assert(hList->uiFirst < hList->uiCapacity);
  
  return hList->apArray[hList->uiFirst];
}

/**
* ArrayList_GetLast
*
* Gets the last list element
*
* @param hList   The list object
*
* @return the last element
*
*/
void *ArrayList_GetLast(ArrayList hList) 
{
  uint32_t uiLast;
  
  assert(hList);
  assert(hList->uiSize > 0);
  assert(hList->apArray);
  assert(hList->uiFirst < hList->uiCapacity);
  
  uiLast = hList->uiFirst + hList->uiSize;
  if (uiLast >= hList->uiCapacity) {
    uiLast -= hList->uiCapacity;
  }
  uiLast = DECREMENT(uiLast, hList->uiCapacity);
  
  return hList->apArray[uiLast];
}

/**
* ArrayList_GetItem
*
* Gets the index-th list element
*
* @param hList    The list object 
* @param uiIndex  The index-th list element 
*
* @return the index-th list element
*
*/
void *ArrayList_GetItem(ArrayList hList, uint32_t uiIndex)
{
  assert(hList);
  assert(hList->apArray);
  assert(uiIndex < hList->uiSize);
  
  uiIndex += hList->uiFirst;
  if(uiIndex >= hList->uiCapacity) {
    uiIndex -= hList->uiCapacity;
  }
  
  assert(uiIndex < hList->uiCapacity);
  
  return hList->apArray[uiIndex];
}

/**
* ArrayList_IsEmpty
*
* True if list is empty
*
* @param hList   The list object
*
* @return true if list is empty, false otherwise
*
*/
bool ArrayList_IsEmpty(ArrayList hList) 
{
  assert(hList);

  return hList->uiSize == 0;
}

/**
* ArrayList_SetAllocationStep
*
* Set the step used when growing the underlying array.
* When the first element is added, an array of uiStep 
* elements is created to store the list. When the list 
* needs to grow even more, room for uiStep more objects is allocated.
*
* @param hList   The list object
* @param uiStep  The allocation step (must be > 0)
*
*
*/
void ArrayList_SetAllocationStep(ArrayList hList, uint32_t uiStep)
{
  assert(hList);
  assert(uiStep > 0);
  
  hList->uiAllocationStep = uiStep;
}


/**
* ArrayList_FreeAll
*
* Call free() on and remove each one of the pointers in the list.
*
* @param list   The list object
*
*/
void ArrayList_FreeAll(ArrayList list)
{
    while (!ArrayList_IsEmpty(list)) {
        free(ArrayList_GetFirst(list));
        ArrayList_UnlinkFirst(list);
    }
}


// Local procedures ------------------------------------------------------------

static bool array_list_grow(ArrayList hList)
{
  uint32_t uiNewCapacity;
  uint32_t uiNewFirst;
  uint32_t uiStep;
  uint32_t uiElementsToShift;
  void **pTmp;

  assert(hList);
  assert(hList->uiSize == hList->uiCapacity);
  assert(hList->uiAllocationStep > 0);

  uiStep        = hList->uiAllocationStep;
  uiNewCapacity = hList->uiCapacity + uiStep;
  FAILIF((pTmp = realloc(hList->apArray, uiNewCapacity * sizeof(pTmp[0]))) == NULL);

  // If the list wraps arount the array, we need to shift some elements to the right
  if (hList->uiFirst + hList->uiSize > hList->uiCapacity) {
    uiNewFirst = hList->uiFirst + uiStep;
    uiElementsToShift = hList->uiCapacity - hList->uiFirst;
  }
  else {
    uiNewFirst = hList->uiFirst;
    uiElementsToShift = 0;
  }

  if (uiElementsToShift > 0) {
    // Shift uiStep positions to the right all the elements after uiFirst
    memmove(pTmp + uiNewFirst, pTmp + hList->uiFirst, uiElementsToShift * sizeof(pTmp[0]));
  }

  // Update the instance
  hList->apArray    = pTmp;
  hList->uiFirst    = uiNewFirst;
  hList->uiCapacity = uiNewCapacity;

  assert(hList->uiFirst < hList->uiCapacity);
  assert(hList->uiSize < hList->uiCapacity);
  return true;

fail:
  return false;
}

