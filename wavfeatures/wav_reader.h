/*
 *  wav_reader.h
 *  
 *  Parser and reader for WAV sound files.
 *  
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef WAVREADER_H
#define WAVREADER_H

#include <stdint.h>
#include <stdbool.h>
#include "file_io.h"

typedef struct WavReader_s *WavReader;

/*
 * Cue point handle
 */
typedef struct CuePoint_s * CuePoint;

/* Wav file reader object */
struct WavReader_s
{
    int  format_tag;
    int  channels;
    int  sampling_rate;
    int  bytes_per_sec;
    int  block_align;
    int  bits_per_sample;
    int64_t  audiodata_size;
    int64_t  data_offset;
    
    CuePoint cues;                    /* List of cue points */
    IO_t io;                          /* I/O context */
};

/**
 * Open a Wav file.
 *
 * @param filename A wav file name
 * @return a WavReader handle or NULL on error
 */
WavReader WavReader_OpenFile(const char* filename);

/**
 * Get the next cue point of a wav file.
 *
 * @param hWav   The WavReader handle
 * @param point   The CuePoint handle. Use NULL to get the first cue point.
 * @return Handle of the next cue point or NULL if there are no more cue points.
 */
CuePoint WavReader_GetNextCuePoint(WavReader hWav, CuePoint point);

/**
 * Read audio data.
 * 
 * The data are returned in PCM format and all channels are interleaved.
 *
 * @param hWav   The WavReader handle
 * @param start_sample Start from this sample n means the nth sample of channel 0.
 * @param buffer Buffer to place the read samples.
 * @param size   The buffer size
 * @return The number of bytes actually read.
 */
int WavReader_ReadAudio(WavReader hWav, uint32_t start_sample, void *buffer, uint32_t size);

/**
 * Close a wav file.
 *
 * @param hWav   The WavReader handle
 */
void WavReader_Close(WavReader hWav);

/**
 * Get the ID of a cue point
 *
 * @param point   The CuePoint handle
 * @return The cue point id
 */
uint32_t WavReader_CuePointGetId(CuePoint point);

/**
 * Get the positions of a cue point
 *
 * @param point   The CuePoint handle
 * @return The sample offset of a cue point.
 */
uint32_t WavReader_CuePointGetPosition(CuePoint point);

/**
 * Get the length (in samples) of a cue point
 *
 * @param point   The CuePoint handle
 * @return The cue point length
 */
uint32_t WavReader_CuePointGetLength(CuePoint point);

/**
 * Get the note associated with a cue point
 *
 * The returned pointer is valid until you close the associated
 * wav file.
 * 
 * @param point   The CuePoint handle
 * @return A string.  
 */
const char * WavReader_CuePointGetNote(CuePoint point);

/**
 * Get the label associated with a cue point
 *
 * The returned pointer is valid until you close the associated
 * wav file.
 * 
 * @param point   The CuePoint handle
 * @return A string.  
 */
const char * WavReader_CuePointGetLabel(CuePoint point);

/**
 * Get the text associated with a cue point
 *
 * The returned pointer is valid until you close the associated
 * wav file.
 * 
 * @param point   The CuePoint handle
 * @return A string.  
 */
const char * WavReader_CuePointGetText(CuePoint point);

#endif // WAVREADER_H
