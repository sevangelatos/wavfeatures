/*
 *  denoiser.c
 *
 *  Remove the influence of noise from a PSD.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */
#include "denoiser.h"
#include <stdint.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include <assert.h>
#include "error_utils.h"
#include "dsp_utils.h"
#include "statistics.h"

#define NOISE_PROFILES_HISTORY 3

struct Denoiser_s {
    int psd_len;
    float noise_level;
    int   last_profile;
    int   psd_profiles_count;
    float *psd_profile;
    float *psd_profiles[NOISE_PROFILES_HISTORY];
};

static void denoizer_update_current_profile(Denoiser dnoise);

Denoiser Denoiser_Create(int psd_len, float noise_threshold)
{
    int i;
    Denoiser dnoise = malloc(sizeof(*dnoise));
    
    if (dnoise) {
        memset(dnoise, 0, sizeof(*dnoise));
        dnoise->psd_len = psd_len;
        dnoise->noise_level = noise_threshold;
        dnoise->psd_profiles_count = 0;
        dnoise->last_profile = -1;
        dnoise->psd_profile = malloc(sizeof(float) * psd_len * (NOISE_PROFILES_HISTORY + 1));
        dnoise->psd_profiles[0] = dnoise->psd_profile + psd_len;
        for (i=1; i < NOISE_PROFILES_HISTORY; i++) {
            dnoise->psd_profiles[i] = dnoise->psd_profiles[i - 1] + psd_len;
        }
    }
    return dnoise;
}


void Denoiser_Destroy(Denoiser dnoise)
{
    if (dnoise) {
        free(dnoise->psd_profile);
        free(dnoise);
    }
}


bool Denoiser_Update(Denoiser dnoise, float *psd, int psd_len, float psd_level)
{
    int i;
    
    assert(dnoise);
    assert(psd_len == dnoise->psd_len);

    // Calculate energy level automatically if it is not readily available.
    if (psd_level == FLT_MIN) {
        psd_level = DspUtils_EnergyFromPSD(psd, psd_len);
    }

    if (psd_level * 0.6 > dnoise->noise_level)
    {
        return false;
    }

    if (psd_level < dnoise->noise_level) {
        dnoise->noise_level = psd_level;
    }
    
    // Count how many profiles we have 
    if (dnoise->psd_profiles_count < NOISE_PROFILES_HISTORY) {
        dnoise->psd_profiles_count += 1;
    }
    
    i = (dnoise->last_profile + 1) % NOISE_PROFILES_HISTORY;
    dnoise->last_profile = i;
    
    memcpy(dnoise->psd_profiles[i], psd, sizeof(psd[0]) * psd_len);
    
    denoizer_update_current_profile(dnoise);
    return true;
}


bool Denoiser_NoiseReducePsd(Denoiser dnoise, float *psd, int psd_len)
{
    int i;
    
    assert(dnoise);
    assert(psd_len == dnoise->psd_len);
    
    if (dnoise->psd_profiles_count < 2) {
        return false;
    }
    
    for (i = 0; i < dnoise->psd_len; i++) {
        psd[i] = MAX(psd[i] - dnoise->psd_profile[i], 0.0f);
    }
    
    return true;
}


/*
 * Update the current noise profile by extracting the median
 * power of each band from the 3 previous noise samples.
*/
static void denoizer_update_current_profile(Denoiser dnoise)
{
    int i;
    float *median    = dnoise->psd_profile;
    float *profile_a = dnoise->psd_profiles[0];
    float *profile_b = dnoise->psd_profiles[1];
    float *profile_c = dnoise->psd_profiles[2];
    
    //fprintf(stderr, "Noise profile_update\n");

    if (dnoise->psd_profiles_count <= 1) {
        profile_b = profile_a;
    }
    
    if (dnoise->psd_profiles_count <= 2) {
        profile_c = profile_a;
    }
    
    for (i = 0; i < dnoise->psd_len; i++) {
        median[i] = Statistics_Median3(profile_a[i], profile_b[i], profile_c[i]);
    }
    
    return;
}
