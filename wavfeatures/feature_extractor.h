/*
 *  feature_extractor.h
 *
 *  Extract feature vectors from audio chunks.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef FEATURE_EXTRACTOR_H
#define FEATURE_EXTRACTOR_H

#include <stdint.h>
#include <stdbool.h>

typedef struct FeatureExtractor_s *FeatureExtractor;

// Class tags
enum ClassLabel_e {
    CLASS_POSITIVE = +1,  // Near cue point marked as positive
    CLASS_NEGATIVE = -1,  // No cue point
    CLASS_TAGGED   =  0   // Near non-positive cue point
};


/*
 * A feature vector encapsulates
 * the features of a sound chunk.
 */
struct FeatureVector_s {
    int64_t sample_offset;  //<< Offset where this feature vector came from
    int     class_label;    //<< placeholder to hold the class of the feature vector
    int     count;          //<< Number of features
    float   *values;        //<< Feature values
    float   _float_align[]; //<< To ease alignment of float values
};

typedef struct FeatureVector_s *FeatureVector;

FeatureExtractor FeatureExtractor_Create(int sample_rate, int fft_len, int history);

FeatureVector FeatureExtractor_Extract(FeatureExtractor fe, float *audio, int len, int64_t sample_offset);

FeatureVector FeatureExtractor_GetFinal(FeatureExtractor fe);

void FeatureExtractor_Destroy(FeatureExtractor fe);


#endif // FEATURE_EXTRACTOR_H
