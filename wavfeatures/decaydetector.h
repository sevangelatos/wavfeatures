/*
 *  decaydetector.h
 *
 *  Detect the decay of a signal by inspecting the
 *  history of the dB power of the signal.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */
#ifndef DECAYDETECTOR_H
#define DECAYDETECTOR_H

typedef struct DecayDetector_s *DecayDetector;


DecayDetector DecayDetector_Create(int length, float threshold_db);


void DecayDetector_Destroy(DecayDetector detect);


float DecayDetector_Update(DecayDetector detect, float rms_db);


#endif // DECAYDETECTOR_H
