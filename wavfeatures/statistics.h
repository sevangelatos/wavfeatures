/*
 *  statistics.h
 *
 *  Statistics related methods and objects.
 *
 *  Created by Spiros Evangelatos.
 *  Copyright 2012. All rights reserved.
 *
 */

#ifndef STATISTICS_H
#define STATISTICS_H

typedef struct MovingMedian_s *MovingMedian;


float Statistics_Mean(float *vector, int len);

float Statistics_MaxAbs(float *vector, int len);

float Statistics_Variance(float *vector, int len, float mean);

float Statistics_StdDev(float *vector, int len, float mean);

float Statistics_Median3(float a, float b, float c);

void MovingMedian_Get(MovingMedian median, float *result, int dims);

void MovingMedian_Update(MovingMedian median, float *values, int dims);

MovingMedian MovingMedian_Create(int window, int dimensions);

void MovingMedian_Destroy(MovingMedian median);


#endif // STATISTICS_H
