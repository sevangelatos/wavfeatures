TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.c \
    wav_reader.c \
    file_io.c \
    dsp_utils.c \
    dsp_linux.c \
    statistics.c \
    denoiser.c \
    attackdetector.c \
    decaydetector.c \
    spectral_features.c \
    feature_extractor.c \
    array_list.c \
    training_set.c

HEADERS += \
    error_utils.h \
    wav_reader.h \
    file_io.h \
    dsp_utils.h \
    statistics.h \
    denoiser.h \
    attackdetector.h \
    decaydetector.h \
    spectral_features.h \
    feature_extractor.h \
    array_list.h \
    training_set.h

LIBS += -lfftw3 -lm
