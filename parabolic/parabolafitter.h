#ifndef PARABOLAFITTER_H
#define PARABOLAFITTER_H

#include <iostream>
#include <vector>
#include <cassert>

class Point2D {
public:
    Point2D(float x, float y): x(x), y(y) {}

    float x;
    float y;
};


class ParabolaFitter
{
public:
    typedef Point2D Point;
    typedef std::vector<Point> PointSet;
    typedef std::vector<bool> PointMask;

    ParabolaFitter();
    
    ~ParabolaFitter();

    /* Load a set of points to be used 
     * for fitting. Fitting can be performed
     * selectively on subsets of this point set. 
     */    
    void load_points(const PointSet &points);

    /* Calculate the parabola with the minimum squared error,
     * based on the points defined by the pointmask
     * subset.
     */
    bool fit_points(const PointMask &subset, float parabola[3]);


private:

    int m_point_count;
    float * m_x;
    float * m_x_2;
    float * m_x_3;
    float * m_x_4;
    float * m_y;
    float * m_yx;
    float * m_yx_2;
};

#endif // PARABOLAFITTER_H
