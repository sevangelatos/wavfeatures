#include "parabolafitter.h"
#include <iostream>
#include <limits>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define NOT_USED(X) ((void)(X))

using namespace std;
using std::numeric_limits;

// Define some types shorthands
typedef ParabolaFitter ParabFitter;
typedef ParabFitter::Point Point;
typedef ParabFitter::PointSet PointSet;
typedef ParabFitter::PointMask PointMask;


static void init_fisher_yates_table (vector<int> &fisher_yates, int size)
{
    fisher_yates.reserve(size);
    for (int i = 0; i < size; ++i) {
        fisher_yates[i] = i;
    }
}

/* A random number in the range [low, high) */
static float random_range(float low, float high)
{
    return low + (high - low) * (std::rand() / (RAND_MAX + 1.0f));
}

static void random_pick(PointMask &selection, int num_picks, vector<int> &fisher_yates)
{    
    int limit = fisher_yates.size();
    
    assert(num_picks <= limit);
    
    for (int i = 0; i < num_picks; ++i) {
        int random = random_range(0, limit);
        int pick = fisher_yates[random];
        
        selection[pick] = true;
        
        // Remove this pick from the options of the next random pick
        limit -= 1;
        int tmp = fisher_yates[limit];
        fisher_yates[limit] = fisher_yates[random];
        fisher_yates[random] = tmp;
    }
    
}

static void pointmask_reset(PointMask &selection)
{
    int size = selection.size();
    
    for (int i = 0; i < size; ++i) {
        selection[i] = false;
    }
}


/* Error function: Total diff of outliers */
float error_diff_sum(PointSet &points, int inlier_count, PointMask &inliers, float parabola[3])
{
    float error = 0;
    int num_points = points.size();

    NOT_USED(inlier_count);
    
    // Include all inliers in the model
    for (int i = 0; i < num_points; ++i) {
        if (inliers[i]) {
            float x = points[i].x;
            float prediction = parabola[0]*x*x + parabola[1]*x + parabola[2];
            
            error += 100 * fabsf(prediction - points[i].y);
        }
    }
    
    return error;
}

/* Error function: Total number of outliers */
float error_outliers(PointSet &points, int inlier_count, PointMask &inliers, float parabola[3])
{
    NOT_USED(inliers);
    NOT_USED(parabola);

    return points.size() - inlier_count;
}


static void print_parabola(float parabola[3])
{
    std::cout << "Parabola: " << parabola[0] << "x^2 + " << parabola[1];
    std::cout << "x + " << parabola[2] << " = 0\n";
}

// Pick inliers according to threshold...
bool is_inlier_threshold(PointSet &points, int n, float model_prediction, float threshold)
{
    return fabs(model_prediction - points[n].y) <= threshold;
}

// Pick inliers by looking also at the other points with the same x
bool is_inlier_smart(PointSet &points, int n, float model_prediction, float threshold)
{
    int lo, hi;
    float x = points[n].x;
    float absdiff = fabs(model_prediction - points[n].y);

    if (absdiff > threshold) {
        return false;
    }

    // Find the range with the points with the same x
    for (lo = n; lo >= 0; --lo) {
        if (points[lo].x != x) {
            lo++;
            break;
        }
    }

    for (hi = n + 1; hi < (int)points.size(); ++hi) {
        if (points[hi].x != x) {
            break;
        }
    }

    // Check if any of the other points with the same x is closer to the
    // predicted value. In that case, we are not among the inliers
    for (int i = lo; i < hi; ++i) {
        if (fabs(model_prediction - points[i].y) < absdiff) {
            return false;
        }
    }

    return true;
}

/* Calculate a good fitting to a parabola
 * based on data with random errors using
 * RANdom SAmple Consensus.
 */
bool RansacParabolic(PointSet &points, int max_iterations, int min_inliers, float parabola[3], float outlier_threshold)
{
    ParabFitter fitter;
    float best_parabola[3];
    float best_error = numeric_limits<float>::infinity();
    const int min_samples = 3; // We need at least 3 numbers for a parabola..
    int num_points = points.size();
    vector<int> fisher_yates(num_points);
    PointMask inliers(num_points, false);
    PointMask consensus_set(num_points, false);
    
    // Load the points in the parabola fitter..
    fitter.load_points(points);
    init_fisher_yates_table(fisher_yates, num_points);
        
    for (int i = 0; i < max_iterations; i++)
    {   
        // Pick 3 samples to construct a parabola...
        pointmask_reset(inliers);
        random_pick(inliers, min_samples, fisher_yates);
        
        // Try to construct a parabola from the 3 picked points..
        if (!fitter.fit_points(inliers, parabola)) {
            continue;
        }
        
        int inlier_count = 0;
        // Include all inliers in the model
        for (int i = 0; i < num_points; ++i) {
            float x = points[i].x;
            float prediction = parabola[0]*x*x + parabola[1]*x + parabola[2];
            
            if (is_inlier_smart(points, i, prediction, outlier_threshold)) {
                inliers[i] = true;
                inlier_count += 1;
            }
        }
        
        // Is the model good enough to evaluate it further?
        if (inlier_count >= min_inliers) {
            // Refine the model by recalculating it with the new inliers
            if (!fitter.fit_points(inliers, parabola)) {
                continue;
            }
                        
            // What is the error using this model?
            float model_error = error_outliers(points, inlier_count, inliers, parabola);
            
            // Is this the best we have found so far?
            if (model_error < best_error) {
                memcpy(best_parabola, parabola, sizeof(parabola));
                best_error = model_error;
                print_parabola(parabola);
            }
        }
        
    } // Try with a different triplet of points
    
    return best_error < numeric_limits<float>::infinity();
}


int main()
{
    int num_points = 30;
    PointSet points;

    float x = -num_points/2;
    for (int i = 0; i < num_points; ++i) {

        if (fabsf(x) > 5.0) {
            points.push_back(Point(x, 9.8 * x * x - 1.0 * x + 5.0 + random_range(0.0, 0.1)));
        }
        else {
            points.push_back(Point(x, 9.8 * x * x - 1.0 * x + 4.0 + random_range(-8, 5)));
            points.push_back(Point(x, 9.8 * x * x - 1.0 * x + 6.0 + random_range(-5, 8)));
        }
        x += 1;
    }

    float parabola[3];
    RansacParabolic(points, 1000, 5, parabola, 2);


    return 0;
}

