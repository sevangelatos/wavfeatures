#include "parabolafitter.h"

#include <iostream>
#include <cassert>

static bool solve_linear_3x3(float system[3][4], float parabola[3]);
static void print_system (float system[3][4]);

ParabolaFitter::ParabolaFitter()
{
    m_point_count = 0;
    m_x = m_x_2 = m_x_3 = m_x_4 = NULL;
    m_yx = m_yx_2 = NULL;
}

ParabolaFitter::~ParabolaFitter()
{
    delete [] m_x;
}

void ParabolaFitter::load_points(const PointSet &points)
{
    int size = points.size();
    
    if (m_x) {
        delete [] m_x;
    }
    
    m_point_count = size;
    m_x = new float[7 * size];
    m_y    = m_x + 1 * size;
    m_x_2  = m_x + 2 * size;
    m_x_3  = m_x + 3 * size;
    m_x_4  = m_x + 4 * size;
    m_yx   = m_x + 5 * size;
    m_yx_2 = m_x + 6 * size;
    
    // Initialize the internal premultiplied arrays
    for (int i = 0; i < size; ++i) {
        float x  = points[i].x;
        float y  = points[i].y;
        float x2 = x * x;
        
        m_x[i] = x;
        m_y[i] = y;
        m_yx[i] = y * x;
        m_x_2[i] = x * x;
        m_x_3[i] = x * x2;
        m_x_4[i] = x2 * x2;
        m_yx_2[i] = y * x2;
    }
}

bool ParabolaFitter::fit_points(const PointMask &subset, float parabola[3])
{
    int total_points = m_point_count;
    float linear_system[3][4];
    float sum_x  = 0;
    float sum_y  = 0;
    float sum_x2 = 0;
    float sum_x3 = 0;
    float sum_x4 = 0;
    float sum_yx = 0;
    float sum_yx2= 0;
    int    sum_1  = 0;
    
    assert((int) subset.size() == total_points);
    
    // Calculate the sums for these points
    for (int i = 0; i < total_points; ++i)
    {
        if (subset[i]) {
            sum_x += m_x[i];
            sum_y += m_y[i];
            sum_x2 += m_x_2[i];
            sum_x3 += m_x_3[i];
            sum_x4 += m_x_4[i];
            sum_yx += m_yx[i];
            sum_yx2 += m_yx_2[i];
            sum_1  += 1;
        }
    }
    
    // Construct a linear system to solve that minimizes the squared distance of the points to a parabola
    linear_system[0][0] = sum_1;   linear_system[0][1] = sum_x;   linear_system[0][2] = sum_x2; linear_system[0][3] = -sum_y;
    linear_system[1][0] = sum_x;   linear_system[1][1] = sum_x2; linear_system[1][2] = sum_x3; linear_system[1][3] = -sum_yx;
    linear_system[2][0] = sum_x2; linear_system[2][1] = sum_x3; linear_system[2][2] = sum_x4; linear_system[2][3] = -sum_yx2;
    
    //print_system(linear_system);
    return solve_linear_3x3(linear_system, parabola);
}


static void print_system (float system[3][4])
{
    for (int i=0; i < 3; i++) {
        for (int j=0; j < 4; j++) {
            std::cout << system[i][j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << "------------------------" << std::endl;
}


static bool solve_linear_3x3(float system[3][4], float parabola[3])
{
    // Make the table upper triangular
    for (int dim = 0; dim < 3; ++dim) {
        for (int eq = dim + 1; eq < 3; ++eq) {
            // The system has no unique solution!
            if (system[dim][dim] == 0.0) {
                return false;
            }
            
            float factor = system[eq][dim] / system[dim][dim];
            for (int c = dim; c < 4; ++c) {
                system[eq][c] -= factor * system[dim][c];
            }
            //print_system(system);
        }
    }
    
    // Solve the system by substitution
    float a = -system[2][3]/system[2][2];
    float b = (-a*system[1][2] -system[1][3]) / system[1][1];
    float c = (-b*system[0][1] -a*system[0][2] -system[0][3]) / system[0][0];
    
    parabola[0] = a;
    parabola[1] = b;
    parabola[2] = c;
    
    return true;
}
